package com.cloudNotificationGCM;

import static com.cloudNotificationGCM.CommonUtilities.SERVER_URL;
import static com.cloudNotificationGCM.CommonUtilities.TAG;
import static com.cloudNotificationGCM.CommonUtilities.displayMessage;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import android.content.Context;
import android.util.Log;

import com.globalenterprice.helper.Constant;
import com.globalenterprisesgroup.R;
import com.google.android.gcm.GCMRegistrar;

/**
 * Helper class used to communicate with the demo server.
 */
public final class ServerUtilities {

	private static final int MAX_ATTEMPTS = 15;
	private static final int BACKOFF_MILLI_SECONDS = 2000;
	private static final Random random = new Random();
	private static Context con;

	/**
	 * Register this account/device pair within the server.
	 * 
	 * @return whether the registration succeeded or not.
	 */
	public static boolean register(final Context context, final String regId) {
		Log.i(TAG, "registering device (regId = " + regId + ")");
		// String serverUrl = SERVER_URL + "/register";
		String serverUrl = SERVER_URL;
		con=context;

		// SharedPreferences shareRegId = context.getSharedPreferences(
		// "RegistrationID", 1);
		// String registrationId = shareRegId.getString("regID", "");
		Constant.REGISTRATIONID = regId;
		System.out.println("MyApplcation " + regId);

		// String android_id = Secure.getString(context.getContentResolver(),
		// Secure.ANDROID_ID);

		/*
		 * String userIdLocal;
		 * if(GetUserProperty.getUserId(context).equalsIgnoreCase("0")) {
		 * userIdLocal = "0"; } else { userIdLocal =
		 * GetUserProperty.getUserId(context); }
		 * 
		 * String localRegId = null; if(!registrationId.equalsIgnoreCase("")) {
		 * localRegId = registrationId; } else
		 * if(!MyApplication.registrationId.equalsIgnoreCase("")) { localRegId =
		 * MyApplication.registrationId; } else
		 * if(!SplashActivityFirst.regId.equalsIgnoreCase("")) { localRegId =
		 * SplashActivityFirst.regId; }
		 */
		/*
		 * try { userIdLocal = WithPHP.bytesToHex((new
		 * WithPHP()).encrypt(userIdLocal)); } catch (Exception e2) {
		 * e2.printStackTrace(); }
		 */

		/*
		 * Map<String, String> params = new HashMap<String, String>();
		 * params.put("userid", userIdLocal); params.put("token", localRegId);
		 * params.put("hardwareid", android_id); params.put("device_type", "A");
		 * 
		 * long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
		 */// Once GCM returns a registration id, we need to register it in the
			// demo server. As the server might be down, we will retry it a
			// couple
			// times.
		for (int i = 1; i <= MAX_ATTEMPTS; i++) {
			Log.d(TAG, "Attempt #" + i + " to register");
			try {
				displayMessage(context, context.getString(
						R.string.server_registering, i, MAX_ATTEMPTS));
				// post(serverUrl, params, context);
								GCMRegistrar.setRegisteredOnServer(context, true);
				String message = context.getString(R.string.server_registered);
				CommonUtilities.displayMessage(context, message);
				return true;
			} catch (Exception e) {
				// Here we are simplifying and retrying on any error; in a real
				// application, it should retry only on unrecoverable errors
				// (like HTTP error code 503).
				/*
				 * Log.e(TAG, "Failed to register on attempt " + i, e); if (i ==
				 * MAX_ATTEMPTS) { break; } try { //Log.d(TAG, "Sleeping for " +
				 * backoff + " ms before retry"); //Thread.sleep(backoff); }
				 * catch (InterruptedException e1) { // Activity finished before
				 * we complete - exit. Log.d(TAG,
				 * "Thread interrupted: abort remaining retries!"); //
				 * Thread.currentThread().interrupt(); return false; }
				 */// increase backoff exponentially
					// backoff *= 2;
			}
		}
		String message = context.getString(R.string.server_register_error,
				MAX_ATTEMPTS);
		CommonUtilities.displayMessage(context, message);
		return false;
	}
	
	
		
	

	/**
	 * Unregister this account/device pair within the server.
	 */
	/*
	 * public static void unregister(final Context context, final String regId)
	 * { Log.i(TAG, "unregistering device (regId = " + regId + ")"); String
	 * serverUrl = SERVER_URL + "/unregister"; Map<String, String> params = new
	 * HashMap<String, String>(); params.put("regId", regId); try {
	 * post(serverUrl, params,context);
	 * GCMRegistrar.setRegisteredOnServer(context, false); String message =
	 * context.getString(R.string.server_unregistered);
	 * CommonUtilities.displayMessage(context, message); } catch (IOException e)
	 * { // At this point the device is unregistered from GCM, but still //
	 * registered in the server. // We could try to unregister again, but it is
	 * not necessary: // if the server tries to send a message to the device, it
	 * will get // a "NotRegistered" error message and should unregister the
	 * device. String message =
	 * context.getString(R.string.server_unregister_error, e.getMessage());
	 * CommonUtilities.displayMessage(context, message); } }
	 */

	/**
	 * Issue a POST request to the server.
	 * 
	 * @param endpoint
	 *            POST address.
	 * @param params
	 *            request parameters.
	 * 
	 * @throws IOException
	 *             propagated from POST.
	 */
	private static void post(String endpoint, Map<String, String> params,
			Context context) throws IOException {
		URL url;
		try {
			url = new URL(endpoint);
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("invalid url: " + endpoint);
		}
		StringBuilder bodyBuilder = new StringBuilder();
		Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
		// constructs the POST body using the parameters
		while (iterator.hasNext()) {
			Entry<String, String> param = iterator.next();
			bodyBuilder.append(param.getKey()).append('=')
					.append(param.getValue());
			if (iterator.hasNext()) {
				bodyBuilder.append('&');
			}
		}
		String body = bodyBuilder.toString();
		// ParsingClass parse = new ParsingClass(context);
		Log.v(TAG, "Posting '" + body + "' to " + endpoint);

		byte[] bytes = body.getBytes();
		HttpURLConnection conn = null;
		try {
			conn = (HttpURLConnection) url.openConnection();
			// conn.setRequestProperty ("authtoken", authtoken);
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setFixedLengthStreamingMode(bytes.length);
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded;charset=UTF-8");
			// post the request
			OutputStream out = conn.getOutputStream();
			out.write(bytes);
			out.close();
			// handle the response
			// int status = conn.getResponseCode();
			// Element ele = parse.startUpConfigrationOfParsing(endpoint+body);

			/*
			 * if(ele == null){ throw new
			 * IOException("Post failed with error code "+403); }
			 */
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
	}
}
