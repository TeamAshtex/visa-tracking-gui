package com.globalenterprisesgroup;

import android.app.Activity;
import android.content.Intent;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.globalenterprice.helper.Constant;

public class Act_Full_Descrption extends Activity {
	TextView tv_title;
	ImageView iv_back;
	WebView wv_description;
	String message, title;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.act_full_description);
		init();
	}

	public Intent newEmailIntent(String address, String subject, String body,
			String cc) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.putExtra(Intent.EXTRA_EMAIL, new String[] { address });
		intent.putExtra(Intent.EXTRA_TEXT, body);
		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		intent.putExtra(Intent.EXTRA_CC, cc);
		intent.setType("message/rfc822");
		return intent;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (wv_description != null)
			wv_description.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (wv_description != null)
			wv_description.onPause();
	}

	private void init() {
		message = getIntent().getStringExtra(Constant.MESSAGE);
		title = getIntent().getStringExtra(Constant.TITLE);
		tv_title = (TextView) findViewById(R.id.tv_title);
		tv_title.setText(title);
		wv_description = (WebView) findViewById(R.id.wv_description);
		wv_description.setBackgroundColor(0x00000000);
		wv_description.loadData(message, "text/html; charset=UTF-8", null);
		wv_description.setWebViewClient(new WebViewClient() {

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				String url_another = url;
				if (url.startsWith("mailto:")) {
					MailTo mt = MailTo.parse(url);
					Intent i = newEmailIntent(mt.getTo(), mt.getSubject(),
							mt.getBody(), mt.getCc());
					startActivity(i);
					view.reload();
				} else if (url.startsWith("tel:")) {
					Intent intent = new Intent(Intent.ACTION_DIAL, Uri
							.parse(url));
					startActivity(intent);
				} else {
					Intent intent = new Intent(Act_Full_Descrption.this,
							Act_WebView.class);
					intent.putExtra(Constant.URL, url_another);
					intent.putExtra(Constant.TITLE, title);
					startActivity(intent);
				}
				return true;
			}
		});
		iv_back = (ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				finish();
			}
		});
	}

}
