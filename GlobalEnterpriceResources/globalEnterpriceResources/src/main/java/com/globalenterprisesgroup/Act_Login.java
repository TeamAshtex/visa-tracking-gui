package com.globalenterprisesgroup;

import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.inputmethod.InputMethodManager;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.globalenterprice.helper.Act_ConnectionDetector;
import com.globalenterprice.helper.Async_Task;
import com.globalenterprice.helper.Constant;
import com.globalenterprisesgroup.R;

public class Act_Login extends Activity implements OnClickListener {

	EditText edit_email, edit_password;
	TextView tv_login;
	String email, uniqueId;
	SharedPreferences prefs, loginprefs;
	SharedPreferences helpscreensplash;
	Editor editor;
	boolean isOpened = false;
	private RelativeLayout rel_bottom;
	ImageView iv_news, iv_contactus, iv_blog;
	Intent intent;
	Act_ConnectionDetector _conn_detector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Constant.changeLang(Act_Login.this);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.act_login);
		init();

	}
		
	private void registerListener() {
		final View activityRootView = getWindow().getDecorView().findViewById(
				android.R.id.content);
		activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(
				new OnGlobalLayoutListener() {
					@Override
					public void onGlobalLayout() {

						int heightDiff = activityRootView.getRootView()
								.getHeight() - activityRootView.getHeight();
						if (heightDiff > 100) {

							if (isOpened == false) {
								if (rel_bottom != null) {
									//rel_bottom.setVisibility(View.INVISIBLE);
								}

							}
							isOpened = true;
						} else if (isOpened == true) {
							if (rel_bottom != null) {
								rel_bottom.setVisibility(View.VISIBLE);
							}

							isOpened = false;
						}
					}
				});
	}

	private void init() {
		prefs = getSharedPreferences(Constant.SHARED_PREF, MODE_PRIVATE);
		loginprefs = getSharedPreferences(Constant.LOGINPREF, MODE_PRIVATE);

		editor = prefs.edit();
		registerListener();
		helpscreensplash = getSharedPreferences(Constant.HELPSHAREDPREF,
				MODE_PRIVATE);
		Editor editor = helpscreensplash.edit();
		editor.putBoolean(Constant.ISHELPCALLED, true);
		editor.commit();
		_conn_detector = new Act_ConnectionDetector(Act_Login.this);
		edit_email = (EditText) findViewById(R.id.edit_email);
		edit_password = (EditText) findViewById(R.id.edit_password);
		tv_login = (TextView) findViewById(R.id.tv_login);
		rel_bottom = (RelativeLayout) findViewById(R.id.rel_bottom);
		iv_blog = (ImageView) findViewById(R.id.iv_blog);
		iv_news = (ImageView) findViewById(R.id.iv_news);
		iv_contactus = (ImageView) findViewById(R.id.iv_contact_us);
		iv_contactus.setBackgroundResource(R.drawable.contact_us);
		iv_news.setBackgroundResource(R.drawable.news);
		iv_blog.setBackgroundResource(R.drawable.blog_updated);
		iv_contactus.setOnClickListener(this);
		iv_news.setOnClickListener(this);
		iv_blog.setOnClickListener(this);

		tv_login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(edit_email.getWindowToken(), 0);
				email = edit_email.getText().toString();
				uniqueId = edit_password.getText().toString();
				// email="admin@globalenterprisesgroup.com";

				// email = "singharjun84@gmail.com";
				// uniqueId="xyz0006abcfg";
				// uniqueId = "xyz0001abcfg";
				// email="james.clarke@globalenterprisesgroup.com";
				// uniqueId="xyz0005abcfg";
				Constant.EMAIL_ID = email;
				Constant.USER_UNIQUE_ID = uniqueId;

				Act_ConnectionDetector _conn = new Act_ConnectionDetector(
						Act_Login.this);
				if (_conn.isConnectingToInternet()) {

					if (email.equalsIgnoreCase("") || uniqueId.equals("")) {
						Toast.makeText(
								getBaseContext(),
								getResources().getString(
										R.string.both_fields_are_required),
								Toast.LENGTH_SHORT).show();
					} else {
						if (validateemail(email)) {
							callLoginApi();
						} else {
							Toast.makeText(
									getBaseContext(),
									getResources()
											.getString(
													R.string.please_enter_valid_email_id),
									Toast.LENGTH_SHORT).show();
						}
					}
				} else {
					Toast.makeText(getBaseContext(),
							getResources().getString(R.string.no_internet),
							Toast.LENGTH_SHORT).show();
				}

			}
		});
	}

	private boolean validateemail(String email) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}

	public void jsonResponse1(String result) {
		Log.e("GCM RESULT", result);
	}

	public void jsonResponse(String result) {
		JSONObject jsonResult = null, jsonresponse;
		JSONArray jsonarray = null;

		try {
			jsonResult = new JSONObject(result);
			jsonarray = jsonResult.optJSONArray(Constant.RESPONSE);

			/*
			 * if (jsonarray != null && jsonarray.length() == 0) {
			 * Toast.makeText( getBaseContext(),
			 * "Oops! The combination of Email address and code doesn't exist.Please try again!"
			 * , Toast.LENGTH_SHORT).show(); return;
			 * 
			 * }
			 */if (jsonarray == null) {
				jsonresponse = jsonResult.getJSONObject(Constant.RESPONSE);
				if (jsonresponse.has(Constant.RESPONSE)) {
					if (!jsonresponse.optString(Constant.RESPONSE).equals("")) {
						// JsonObject temp_obj=jsonresponse.getString("status");
						/*
						 * Toast.makeText( getBaseContext(),
						 * "Oops! The combination of Email address and code doesn't exist.Please try again!"
						 * , Toast.LENGTH_SHORT).show();
						 */
						JSONObject temp_obj = jsonresponse
								.getJSONObject(Constant.RESPONSE);
						String message = temp_obj.getString(Constant.MESSAGE);
						if (message
								.equalsIgnoreCase("Whoops! Those details do not appear to be correct. Please check your details and try again.")) {
							Toast.makeText(getBaseContext(), message,
									Toast.LENGTH_SHORT).show();
							return;
						} else if (message
								.equalsIgnoreCase("Whoops! You do not appear to have any cases with us.")) {
							Editor editor1 = loginprefs.edit();
							editor1.putString(Constant.LOGINUSEREMAIL,
									Constant.EMAIL_ID);
							editor1.putString(Constant.LOGINPASS,
									Constant.USER_UNIQUE_ID);
							editor1.commit();
							editor.putInt(Constant.SIZE, 0);
							editor.commit();
						}

					}
				}
			}

			if (jsonarray != null) {
				for (int i = 0; i < jsonarray.length(); i++) {
					JSONObject jobj = jsonarray.getJSONObject(i);
					String errorcode = jobj.optString(Constant.ERRORCODE);
					if (errorcode != null && !errorcode.equals("")) {
						String message = jobj.optString(Constant.MESSAGE);
						Toast.makeText(getBaseContext(), message,
								Toast.LENGTH_SHORT).show();
						return;
					}

					editor.putString(Constant.ID + i, jobj.toString());

				}
				Editor editor1 = loginprefs.edit();
				editor1.putString(Constant.LOGINUSEREMAIL, Constant.EMAIL_ID);
				editor1.putString(Constant.LOGINPASS, Constant.USER_UNIQUE_ID);
				editor1.commit();
				editor.putInt(Constant.SIZE, jsonarray.length());
				editor.commit();
			}

			startActivity(new Intent(Act_Login.this, Act_Home.class));
			finish();

		} catch (Exception e) {
			/*
			 * Toast.makeText( getBaseContext(),
			 * "Oops! The combination of Email address and code doesn't exist.Please try again!"
			 * , Toast.LENGTH_SHORT).show();
			 */
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.iv_news:
			iv_news.setBackgroundResource(R.drawable.news_sel);

			intent = new Intent(Act_Login.this, Act_Blog_News.class);
			intent.putExtra(Constant.URL, Constant.NEWSURL);
			intent.putExtra(Constant.TITLE,
					getResources().getString(R.string.news));
			intent.putExtra(Constant.ISFROM, Constant.ISFROMNEWS);
			startActivity(intent);

			break;
		case R.id.iv_case_tracker:

			break;

		case R.id.iv_contact_us:
			iv_contactus.setBackgroundResource(R.drawable.contact_us_sel);
			startActivity(new Intent(Act_Login.this, Act_ContactUs.class));

			break;
		case R.id.iv_blog:
			iv_blog.setBackgroundResource(R.drawable.blog_sel_updated);
			intent = new Intent(Act_Login.this, Act_Blog_News.class);
			intent.putExtra(Constant.URL, Constant.BLOGURL);
			intent.putExtra(Constant.ISFROM, Constant.ISFROMBLOG);
			intent.putExtra(Constant.TITLE,
					getResources().getString(R.string.Blog));
			if (_conn_detector.isConnectingToInternet())
				startActivity(intent);
			else
				Toast.makeText(getBaseContext(),
						"Internet connection not available.",
						Toast.LENGTH_SHORT).show();
			break;

		default:
			break;
		}

	}

	private void callLoginApi() {
		try {

			new Async_Task(this, true).execute(email, uniqueId, Act_Login.this);
		} catch (Exception e) {
			Log.e("Exception for Login", e.toString());
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		iv_contactus.setBackgroundResource(R.drawable.contact_us);
		iv_news.setBackgroundResource(R.drawable.news);
		iv_blog.setBackgroundResource(R.drawable.blog_updated);

	}

}