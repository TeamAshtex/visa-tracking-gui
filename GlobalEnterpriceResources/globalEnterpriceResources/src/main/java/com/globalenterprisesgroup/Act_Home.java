package com.globalenterprisesgroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.classes.SurveryMonkeyModel;
import com.globalenterprice.helper.Act_ConnectionDetector;
import com.globalenterprice.helper.Async_Task;
import com.globalenterprice.helper.CaseListAdapter;
import com.globalenterprice.helper.Constant;
import com.globalenterprice.helper.DatabaseHelper;

@SuppressLint("NewApi")
@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class Act_Home extends Activity implements OnClickListener,
		AnimationListener {
	ImageView iv_news, iv_contactus, iv_resources, iv_case_tracker,
			iv_select_Case, iv_forum, iv_checklist, iv_message, iv_refresh,
			iv_logout;
	public static ListView lv_case;
	int time;
	Timer timer;
	boolean iscasedeleted = false, home_noti_flag = false;
	static boolean casedeleted = false;
	boolean iscurrentpage = false;
	boolean isFromMessage = false;

	ImageView iv_back, iv_preparing, iv_allocated, iv_awaiting, iv_decision,
			iv_pending, iv_background;
	TextView tv_title, tv_case;
	ImageView iv_blog, iv_flag;
	Intent intent;
	Act_ConnectionDetector _conn_detector;
	DatabaseHelper dbHelper;
	SharedPreferences prefs;
	private ArrayList<String> casenum;
	Animation animFadein, animFadeOut, animFadinAnother;
	CaseListAdapter adapter;
	private LinearLayout ll_list_root;
	private RelativeLayout rel_sel_case, rel_root_stages;
	int flag = 0;
	Editor editor;
	static String refreshstage = "", notification_case_id,
			message_notification, lv_stage;
	private ScrollView sc_main;
	private View view_clicked;
	private HashMap<String, String> stagewithvalue = new HashMap<String, String>();
	private SurveryMonkeyModel monkeyClass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Constant.changeLang(Act_Home.this);

		setContentView(R.layout.act_home);
		init();

	}

	private void callGCMAPI() {
		if (prefs.getString(Constant.ISDEVICETOKEN, "").equals("")) {
			if (Constant.REGISTRATIONID != null
					&& !Constant.REGISTRATIONID.equals("")) {
				new Async_Task(Act_Home.this, false).execute(Constant.EMAIL_ID,
						Constant.DEVICETYPEID, Act_Home.this,
						Constant.REGISTRATIONID, "gcmapi");
			}
		}
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@SuppressLint("NewApi")
	private void init() {
		prefs = getSharedPreferences(Constant.SHARED_PREF, MODE_PRIVATE);
		editor = prefs.edit();

		initHashMap();
		callGCMAPI();
		callInitialCaseTrackerApi();
		try {
			dbHelper = new DatabaseHelper(getApplicationContext());

			dbHelper.getWritableDatabase();
		} catch (Exception e) {

		}
		message_notification = getIntent()
				.getStringExtra("messagenotification");
		sc_main = (ScrollView) findViewById(R.id.main_scroll);
		iv_allocated = (ImageView) findViewById(R.id.iv_allocated);
		iv_pending = (ImageView) findViewById(R.id.iv_pending);
		iv_preparing = (ImageView) findViewById(R.id.iv_preparing);
		iv_decision = (ImageView) findViewById(R.id.iv_decision);
		iv_awaiting = (ImageView) findViewById(R.id.iv_awaiting);
		iv_background = (ImageView) findViewById(R.id.iv_background);
		iv_flag = (ImageView) findViewById(R.id.iv_flag);

		iv_preparing.setBackground(null);
		iv_preparing.setBackgroundResource(R.drawable.preparing);

		iv_pending.setBackground(null);
		iv_pending.setBackgroundResource(R.drawable.pending);

		iv_awaiting.setBackground(null);
		iv_awaiting.setBackgroundResource(R.drawable.allocation);

		iv_allocated.setBackground(null);
		iv_allocated.setBackgroundResource(R.drawable.caseimage);

		iv_decision.setBackground(null);
		iv_decision.setBackgroundResource(R.drawable.blank);

		iv_background.setBackground(null);
		iv_background.setBackgroundResource(R.drawable.white_background);

		iv_preparing.setOnClickListener(this);
		iv_pending.setOnClickListener(this);
		iv_decision.setOnClickListener(this);
		iv_awaiting.setOnClickListener(this);
		iv_allocated.setOnClickListener(this);
		iv_flag.setOnClickListener(this);

		animFadein = AnimationUtils.loadAnimation(getApplicationContext(),
				R.anim.fade_in);
		animFadinAnother = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.fade_in_another);
		animFadeOut = AnimationUtils.loadAnimation(getApplicationContext(),
				R.anim.fade_out);

		animFadein.setAnimationListener(this);
		animFadeOut.setAnimationListener(this);
		animFadinAnother.setAnimationListener(this);
		tv_case = (TextView) findViewById(R.id.tv_case);
		rel_root_stages = (RelativeLayout) findViewById(R.id.rel_root_stages);
		ll_list_root = (LinearLayout) findViewById(R.id.ll_root);
		rel_sel_case = (RelativeLayout) findViewById(R.id.rel_sel_case);
		_conn_detector = new Act_ConnectionDetector(Act_Home.this);
		iv_blog = (ImageView) findViewById(R.id.iv_blog);
		iv_refresh = (ImageView) findViewById(R.id.iv_refresh);
		iv_refresh.setVisibility(View.VISIBLE);
		iv_back = (ImageView) findViewById(R.id.iv_back);
		iv_logout = (ImageView) findViewById(R.id.iv_logout);
		iv_logout.setVisibility(View.VISIBLE);

		iv_logout.setOnClickListener(this);

		tv_title = (TextView) findViewById(R.id.tv_title);
		tv_title.setText(getResources().getString(R.string.casetracker));
		iv_back.setVisibility(View.INVISIBLE);
		lv_case = (ListView) findViewById(R.id.lv_cases);
		lv_case.setVerticalFadingEdgeEnabled(false);
		iv_news = (ImageView) findViewById(R.id.iv_news);
		iv_case_tracker = (ImageView) findViewById(R.id.iv_case_tracker);
		iv_resources = (ImageView) findViewById(R.id.iv_resources);
		iv_contactus = (ImageView) findViewById(R.id.iv_contact_us);
		//iv_contactus.setBackgroundResource(R.drawable.contact_us);
		//iv_news.setBackgroundResource(R.drawable.news);
		//iv_blog.setBackgroundResource(R.drawable.blog_updated);
		iv_select_Case = (ImageView) findViewById(R.id.iv_select_case);
		iv_forum = (ImageView) findViewById(R.id.iv_forum);
		iv_checklist = (ImageView) findViewById(R.id.iv_checklist);
		iv_message = (ImageView) findViewById(R.id.iv_message);
		iv_select_Case.setBackgroundResource(R.drawable.dragdown);
		//iv_case_tracker.setBackgroundResource(R.drawable.case_tracker_sel);

		iv_preparing.setDrawingCacheEnabled(true);
		iv_preparing.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				boolean flag1 = false;
				try {

					Bitmap bmp = Bitmap.createBitmap(v.getDrawingCache());
					int color = bmp.getPixel((int) event.getX(),
							(int) event.getY());
					if (color == Color.TRANSPARENT) {
						flag1 = false;
						return false;
					} else {
						// code to execute
						if (event.getPointerCount() > 1)
							return true;
						flag1 = true;
						showmessage();
						return true;
					}
				} catch (Exception e) {

				}
				return flag1;
			}

		});

		iv_blog.setOnClickListener(this);
		iv_news.setOnClickListener(this);
		iv_case_tracker.setOnClickListener(this);
		iv_resources.setOnClickListener(this);
		iv_contactus.setOnClickListener(this);
		// iv_select_Case.setOnClickListener(this);
		iv_forum.setOnClickListener(this);
		iv_checklist.setOnClickListener(this);
		iv_message.setOnClickListener(this);
		iv_refresh.setOnClickListener(this);
		rel_sel_case.setOnClickListener(this);
		rel_root_stages.setOnClickListener(this);
		casenum = new ArrayList<String>();
		adapter = new CaseListAdapter(this, casenum);

		lv_case.setAdapter(adapter);
		lv_case.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				view.setBackgroundColor(getResources().getColor(
						R.color.list_Sel_color));
				view_clicked = view;

				Constant.position = position;
				String casetext = casenum.get(position);
				SharedPreferences posprefs = getSharedPreferences(
						Constant.CASESHAREDPREF, MODE_PRIVATE);
				Editor editor = posprefs.edit();
				editor.putString(Constant.CASESTRING, casetext);
				editor.commit();
				String stage = "";
				int size = prefs.getInt(Constant.SIZE, 0);
				for (int i = 0; i < size; i++) {
					try {
						JSONObject jobj = new JSONObject(prefs.getString(
								Constant.ID + i, ""));
						String caseid = jobj.getString(Constant.ID);
						String casenum = jobj
								.getString(Constant.CASENUMBERVALUE);
						if (casenum.equalsIgnoreCase(casetext)) {
							stage = jobj.getString(Constant.STAGE);
							startAndDisplayImageViewWithAnimations(stage);
							Constant.CASE_ID = caseid;
							Constant.CASE_HASHKEY = jobj
									.optString(Constant.SURVEYHASH);
						}
					} catch (Exception e) {

					}
				}

				ll_list_root.startAnimation(animFadeOut);
				// sc_main.setVerticalScrollBarEnabled(false);
				tv_case.setText(casetext);
				iv_select_Case.setBackgroundResource(R.drawable.dragdown);
				iv_flag.setVisibility(View.VISIBLE);

			}
		});

		Utility.setListViewHeightBasedOnChildren(lv_case);
		callLangChange();

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		Constant.position = -1;
		refreshstage = "";
		casedeleted = false;
		iscasedeleted = false;
		Constant.CASE_ID = "";
		SharedPreferences posprefs = getSharedPreferences(
				Constant.CASESHAREDPREF, MODE_PRIVATE);
		Editor editor1 = posprefs.edit();
		editor1.clear().commit();
		Log.e("In On Destroy", "In on destroy");

	}

	private void startAndDisplayImageViewWithAnimations(String stage) {
		try {
			JSONObject jobj = new JSONObject(prefs.getString(Constant.ID
					+ Constant.position, ""));
			// String stage = jobj.getString(Constant.STAGE);
			if (stage.equals("null")) {
				loadimageswithanimation(6);

			} else {

				int pos = findpositionofstage(stage);
				loadimageswithanimation(pos);
			}
		} catch (Exception e) {

		}

	}

	private void loadAllImages() {
		// rel_root_stages.startAnimation(animFadein);

		iv_preparing.setEnabled(true);
		iv_allocated.setEnabled(true);
		iv_awaiting.setEnabled(true);
		iv_decision.setEnabled(true);
		iv_pending.setEnabled(true);
		iv_message.setEnabled(true);
		iv_checklist.setEnabled(true);
		iv_forum.setEnabled(true);
		iv_resources.setEnabled(true);
		flag = 1;
		time = 0;
		if (timer != null) {
			iv_background.clearAnimation();
			timer.cancel();

		}

		TimerTask tt = new TimerTask() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				if (time == 0) {
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							iv_preparing.startAnimation(animFadinAnother);
							iv_preparing.setVisibility(View.VISIBLE);
						}
					});

				}
				if (time == 300) {
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							iv_preparing.clearAnimation();
							iv_pending.startAnimation(animFadinAnother);
							iv_pending.setVisibility(View.VISIBLE);

						}
					});

				}
				if (time == 600) {
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							iv_pending.clearAnimation();
							iv_awaiting.startAnimation(animFadinAnother);
							iv_awaiting.setVisibility(View.VISIBLE);
						}
					});

				}
				if (time == 600) {
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							iv_awaiting.clearAnimation();
							iv_allocated.startAnimation(animFadinAnother);
							iv_allocated.setVisibility(View.VISIBLE);
						}
					});

				}
				if (time == 900) {
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							iv_allocated.clearAnimation();
							iv_decision.startAnimation(animFadinAnother);
							iv_decision.setVisibility(View.VISIBLE);
						}
					});

				}
				if (time == 1200) {
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							iv_decision.clearAnimation();
							iv_background.startAnimation(animFadinAnother);
							iv_background.setVisibility(View.VISIBLE);
						}
					});
				}
				time += 300;
			}
		};
		timer = new Timer();
		timer.scheduleAtFixedRate(tt, 0, 300);

	}

	private void makevisibilitygone() {
		iv_preparing.setVisibility(View.INVISIBLE);
		iv_pending.setVisibility(View.INVISIBLE);

		iv_awaiting.setVisibility(View.INVISIBLE);

		iv_allocated.setVisibility(View.INVISIBLE);

		iv_decision.setVisibility(View.INVISIBLE);
		iv_background.setVisibility(View.INVISIBLE);
	}

	private void loadimageswithanimation(int position) {
		switch (position) {
		case 0:
			iv_preparing.setBackgroundResource(R.drawable.preparing_sel);
			iv_pending.setBackgroundResource(R.drawable.pending);

			iv_awaiting.setBackgroundResource(R.drawable.allocation);

			iv_allocated.setBackgroundResource(R.drawable.caseimage);

			iv_decision.setBackgroundResource(R.drawable.blank);
			iv_background.setBackgroundResource(R.drawable.white_background);
			makevisibilitygone();

			loadAllImages();

			break;
		case 1:
			iv_preparing.setBackgroundResource(R.drawable.preparing_sel);

			iv_pending.setBackgroundResource(R.drawable.pending_sel);
			iv_awaiting.setBackgroundResource(R.drawable.allocation);

			iv_allocated.setBackgroundResource(R.drawable.caseimage);

			iv_decision.setBackgroundResource(R.drawable.blank);
			iv_background.setBackgroundResource(R.drawable.white_background);
			makevisibilitygone();
			loadAllImages();

			break;
		case 2:
			iv_preparing.setBackgroundResource(R.drawable.preparing_sel);

			iv_pending.setBackgroundResource(R.drawable.pending_sel);

			iv_awaiting.setBackgroundResource(R.drawable.allocation_sel);
			iv_allocated.setBackgroundResource(R.drawable.caseimage);

			iv_decision.setBackgroundResource(R.drawable.blank);
			iv_background.setBackgroundResource(R.drawable.white_background);
			makevisibilitygone();
			loadAllImages();

			break;
		case 3:
			iv_preparing.setBackgroundResource(R.drawable.preparing_sel);

			iv_pending.setBackgroundResource(R.drawable.pending_sel);

			iv_awaiting.setBackgroundResource(R.drawable.allocation_sel);

			iv_allocated.setBackgroundResource(R.drawable.case_sel);
			iv_decision.setBackgroundResource(R.drawable.blank);
			iv_background.setBackgroundResource(R.drawable.white_background);
			makevisibilitygone();
			loadAllImages();

			break;
		case 4:
			iv_preparing.setBackgroundResource(R.drawable.preparing_sel);

			iv_pending.setBackgroundResource(R.drawable.pending_sel);

			iv_awaiting.setBackgroundResource(R.drawable.allocation_sel);

			iv_allocated.setBackgroundResource(R.drawable.case_sel);

			iv_decision.setBackgroundResource(R.drawable.unfavorable);
			makevisibilitygone();
			loadAllImages();

			break;
		case 5:
			iv_preparing.setBackgroundResource(R.drawable.preparing_sel);

			iv_pending.setBackgroundResource(R.drawable.pending_sel);

			iv_awaiting.setBackgroundResource(R.drawable.allocation_sel);

			iv_allocated.setBackgroundResource(R.drawable.case_sel);

			iv_decision.setBackgroundResource(R.drawable.decision);
			iv_background.setBackgroundResource(R.drawable.white_background);
			makevisibilitygone();
			loadAllImages();

			break;
		case 6:
			iv_preparing.setBackgroundResource(R.drawable.preparing);

			iv_pending.setBackgroundResource(R.drawable.pending);

			iv_awaiting.setBackgroundResource(R.drawable.allocation);

			iv_allocated.setBackgroundResource(R.drawable.caseimage);

			iv_decision.setBackgroundResource(R.drawable.blank);
			iv_background.setBackgroundResource(R.drawable.white_background);
			makevisibilitygone();
			loadAllImages();

		default:
			break;
		}
	}

	private void initHashMap() {

		stagewithvalue.put("Preparing case for lodgement", "0");
		stagewithvalue.put("Pending payment", "1");
		stagewithvalue.put("Awaiting allocation to a case officer", "2");
		stagewithvalue.put("Case officer allocated and processing", "3");
		stagewithvalue.put("Unfavourable Decision / Withdrawn", "4");
		stagewithvalue.put("Decision complete", "5");
		stagewithvalue.put("Unfavourable Decision / Withdrawn", "6");

	}

	private int findpositionofstage(String stagename) {
		int pos = 0;
		for (int i = 0; i < stagewithvalue.size(); i++) {
			if (stagewithvalue.containsKey(stagename)) {
				pos = Integer.parseInt(stagewithvalue.get(stagename));
				return pos;
			}
		}
		return pos;
	}

	private void initCaseTrackerValues() {
		updatecasenum();

	}

	public static class Utility {
		public static void setListViewHeightBasedOnChildren(ListView listView) {
			ListAdapter listAdapter = listView.getAdapter();
			if (listAdapter == null) {
				return;
			}

			int totalHeight = listView.getPaddingTop()
					+ listView.getPaddingBottom();
			for (int i = 0; i < listAdapter.getCount(); i++) {
				View listItem = listAdapter.getView(i, null, listView);
				if (listItem instanceof ViewGroup)
					listItem.setLayoutParams(new LayoutParams(
							LayoutParams.WRAP_CONTENT,
							LayoutParams.WRAP_CONTENT));
				listItem.measure(0, 0);
				totalHeight += listItem.getMeasuredHeight();
			}

			ViewGroup.LayoutParams params = listView.getLayoutParams();
			params.height = totalHeight
					+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
			listView.setLayoutParams(params);
		}
	}

	public void changeLang(String lang) {
		if (lang.equalsIgnoreCase("en"))
			Constant.lang = Constant.eng_lang;
		else
			Constant.lang = Constant.ch_lang;

		/*
		 * Locale locale = new Locale(lang); Locale.setDefault(locale);
		 * Configuration config = new Configuration(); config.locale = locale;
		 * getBaseContext().getResources().updateConfiguration(config,
		 * getBaseContext().getResources().getDisplayMetrics());
		 */startActivity(new Intent(this, Act_Home.class));

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.iv_news:
			//iv_news.setBackgroundResource(R.drawable.news_sel);
			intent = new Intent(Act_Home.this, Act_Blog_News.class);
			intent.putExtra(Constant.URL, Constant.NEWSURL);
			intent.putExtra(Constant.TITLE,
					getResources().getString(R.string.news));
			intent.putExtra(Constant.ISFROM, Constant.ISFROMNEWS);
			startActivity(intent);

			break;
		case R.id.iv_case_tracker:

			break;
		case R.id.rel_root_stages:

			break;
		case R.id.iv_allocated:
			showmessage();
			break;
		case R.id.iv_preparing:
			break;
		case R.id.iv_awaiting:
			showmessage();
			break;
		case R.id.iv_pending:
			showmessage();
			break;
		case R.id.iv_decision:
			showmessage();
			break;
		case R.id.iv_refresh:
			Act_ConnectionDetector _conn = new Act_ConnectionDetector(
					Act_Home.this);
			if (_conn.isConnectingToInternet()) {

				callCaseTrackerApi();
			}

			else {
				Toast.makeText(getBaseContext(),
						getResources().getString(R.string.no_internet),
						Toast.LENGTH_SHORT).show();
			}

			break;
		case R.id.rel_sel_case:
			if (lv_case.getVisibility() == View.VISIBLE) {
				iv_preparing.setEnabled(true);
				iv_allocated.setEnabled(true);
				iv_awaiting.setEnabled(true);
				iv_decision.setEnabled(true);
				iv_pending.setEnabled(true);
				iv_message.setEnabled(true);
				iv_checklist.setEnabled(true);
				iv_forum.setEnabled(true);
				iv_resources.setEnabled(true);
				sc_main.setVerticalScrollBarEnabled(true);

				ll_list_root.startAnimation(animFadeOut);
				lv_case.setVisibility(View.GONE);
				iv_select_Case.setBackgroundResource(R.drawable.dragdown);
				iv_flag.setVisibility(View.VISIBLE);

			} else {

				adapter.notifyDataSetChanged();
				sc_main.setVerticalScrollBarEnabled(false);
				ll_list_root.startAnimation(animFadein);
				lv_case.setVisibility(View.VISIBLE);
				iv_preparing.setEnabled(false);
				iv_allocated.setEnabled(false);
				iv_awaiting.setEnabled(false);
				iv_decision.setEnabled(false);
				iv_pending.setEnabled(false);

				iv_message.setEnabled(false);
				iv_checklist.setEnabled(false);
				iv_forum.setEnabled(false);
				iv_resources.setEnabled(false);
				iv_select_Case.setBackgroundResource(R.drawable.upward_arrow);
				//iv_flag.setVisibility(View.GONE);
			}

			break;
		case R.id.iv_checklist:

			if (Constant.CASE_ID != null
					&& Constant.CASE_ID.equalsIgnoreCase("")) {
				int size = prefs.getInt(Constant.SIZE, 0);
				if (size > 0) {
					Toast.makeText(
							getBaseContext(),
							getResources().getString(
									R.string.please_select_your_case),
							Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(
							getBaseContext(),
							getResources().getString(
									R.string.no_cases_available),
							Toast.LENGTH_SHORT).show();
				}
			} else {
				startActivity(new Intent(Act_Home.this, Act_CheckList.class));
			}
			break;
		case R.id.iv_forum:

			/*
			 * intent = new Intent(Act_Home.this, Act_WebView.class);
			 * intent.putExtra(Constant.TITLE,
			 * getResources().getString(R.string.forum));
			 * intent.putExtra(Constant.URL, Constant.INFORMATIONURL); if
			 * (_conn_detector.isConnectingToInternet()) startActivity(intent);
			 * else Toast.makeText(getBaseContext(),
			 * getResources().getString(R.string.no_internet),
			 * Toast.LENGTH_SHORT).show();
			 */
			if (Constant.CASE_ID != null
					&& Constant.CASE_ID.equalsIgnoreCase("")) {
				int size = prefs.getInt(Constant.SIZE, 0);
				if (size > 0) {
					Toast.makeText(
							getBaseContext(),
							getResources().getString(
									R.string.please_select_your_case),
							Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(
							getBaseContext(),
							getResources().getString(
									R.string.no_cases_available),
							Toast.LENGTH_SHORT).show();
				}
			} else {
				if (_conn_detector.isConnectingToInternet()) {
					if (Constant.CASE_HASHKEY.equals(null)
							|| Constant.CASE_HASHKEY.equals("")
							|| Constant.CASE_HASHKEY.equals("null")) {
						Toast.makeText(
								getBaseContext(),
								getResources().getString(
										R.string.not_found_survey),
								Toast.LENGTH_SHORT).show();

					} else {
						monkeyClass = new SurveryMonkeyModel(Act_Home.this);
						iv_forum.setEnabled(false);
					}
				} else
					Toast.makeText(getBaseContext(),
							getResources().getString(R.string.no_internet),
							Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.iv_resources:

			intent = new Intent(Act_Home.this, Act_WebView.class);
			intent.putExtra(Constant.TITLE,
					getResources().getString(R.string.resources));
			intent.putExtra(Constant.URL, Constant.RESOURCESURL);
			if (_conn_detector.isConnectingToInternet())
				startActivity(intent);
			else
				Toast.makeText(getBaseContext(),
						getResources().getString(R.string.no_internet),
						Toast.LENGTH_SHORT).show();

			break;

		case R.id.iv_logout:
			AlertDialog.Builder builder = new AlertDialog.Builder(Act_Home.this);
			builder.setTitle(getResources().getString(R.string.confirm_logout));
			builder.setMessage(getResources().getString(
					R.string.do_you_really_want_to_logout));
			builder.setPositiveButton(getResources().getString(R.string.yes),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							calllogoutapi();
						}
					});
			builder.setNegativeButton(getResources().getString(R.string.no),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});
			builder.create();
			builder.show();

			break;
		case R.id.iv_contact_us:
			//iv_contactus.setBackgroundResource(R.drawable.contact_us_sel);
			startActivity(new Intent(Act_Home.this, Act_ContactUs.class));

			break;
		case R.id.iv_message:
			if (Constant.CASE_ID != null
					&& Constant.CASE_ID.equalsIgnoreCase("")) {
				int size = prefs.getInt(Constant.SIZE, 0);
				if (size > 0) {
					Toast.makeText(
							getBaseContext(),
							getResources().getString(
									R.string.please_select_your_case),
							Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(
							getBaseContext(),
							getResources().getString(
									R.string.no_cases_available),
							Toast.LENGTH_SHORT).show();
				}
			} else {
				Intent messageactivity = new Intent(Act_Home.this,
						Act_Message.class);
				String casetext = tv_case.getText().toString();
				messageactivity.putExtra(Constant.MESSAGECASNUMBER, casetext);
				startActivity(messageactivity);
			}
			break;
		case R.id.iv_blog:
			//iv_blog.setBackgroundResource(R.drawable.blog_sel_updated);
			intent = new Intent(Act_Home.this, Act_Blog_News.class);
			intent.putExtra(Constant.URL, Constant.BLOGURL);
			intent.putExtra(Constant.TITLE,
					getResources().getString(R.string.Blog));
			intent.putExtra(Constant.ISFROM, Constant.ISFROMBLOG);
			if (_conn_detector.isConnectingToInternet())
				startActivity(intent);
			else
				Toast.makeText(getBaseContext(),
						getResources().getString(R.string.no_internet),
						Toast.LENGTH_SHORT).show();
			break;

		case R.id.iv_flag:

			changeLang(String.valueOf(iv_flag.getTag()));
			break;
		default:
			break;
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		// This is where you consume the respondent data returned by the
		// SurveyMonkey Mobile Feedback SDK
		// In this example, we deserialize the user's response, check to see if
		// they gave our app 4 or 5 stars, and then provide visual prompts to
		// the user based on their response
		super.onActivityResult(requestCode, resultCode, intent);
		iv_forum.setEnabled(true);

		monkeyClass.activityResult(requestCode, resultCode, intent);
	}

	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub

	}

	/*
	 * 
	 * startAndDisplayImageViewWithAnimations(stage);
	 */

	private void showMessageDialog() {

		DialogInterface.OnClickListener onClick = new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				if (which == DialogInterface.BUTTON_POSITIVE) {
					Intent messageactivity = new Intent(Act_Home.this,
							Act_Message.class);
					messageactivity.putExtra(Constant.MESSAGE, getResources()
							.getString(R.string.dialog_message));
					String casetext = tv_case.getText().toString();
					messageactivity.putExtra(Constant.MESSAGECASNUMBER,
							casetext);
					startActivity(messageactivity);
				} else if (which == DialogInterface.BUTTON_NEGATIVE) {
					dialog.dismiss();
				}
			}

		};
		Builder builder = new AlertDialog.Builder(this);

		builder.setMessage(getResources().getString(R.string.dialog_message));

		builder.setCancelable(true);
		builder.setPositiveButton(getResources().getString(R.string.yes),
				onClick);
		builder.setNegativeButton(getResources().getString(R.string.no),
				onClick);

		AlertDialog dialog = builder.create();

		dialog.show();

	}

	@Override
	public void onAnimationEnd(Animation animation) {
		// TODO Auto-generated method stub
		if (animation == animFadeOut) {
			lv_case.setVisibility(View.GONE);

		}

	}

	private void calllogoutapi() {
		new Async_Task(Act_Home.this, true).execute(Constant.EMAIL_ID,
				Constant.DEVICETYPEID, Act_Home.this,
				getResources().getString(R.string.logout));
	}

	private void onNotificationUpdae() {
		SharedPreferences posprefs = getSharedPreferences(
				Constant.CASESHAREDPREF, MODE_PRIVATE);
		String caseid = posprefs.getString(Constant.CASESTRING, "");

		updatecasenum();
		adapter.setData(casenum);
		Utility.setListViewHeightBasedOnChildren(lv_case);
		int size = prefs.getInt(Constant.SIZE, -1);

		for (int i = 0; i < size; i++) {
			try {
				JSONObject jobj = new JSONObject(prefs.getString(Constant.ID
						+ i, ""));
				String casenumber = jobj.getString(Constant.CASENUMBERVALUE);
				if (casenumber.equalsIgnoreCase(caseid)) {
					String stage = jobj.getString(Constant.STAGE);
					String id = jobj.getString(Constant.ID);
					Constant.CASE_ID = id;
					loadimageswithanimation(findpositionofstage(stage));
					tv_case.setText(casenumber);
					for (int j = 0; j < casenum.size(); j++) {
						if (casenum.get(j).equals(casenumber)) {

							View view = lv_case.getChildAt(j);
							view.setBackgroundColor(getResources().getColor(
									R.color.list_Sel_color));
							view_clicked = view;

							Constant.position = j;

						}

					}

				}
			} catch (Exception e) {
				Log.e("Message Screen notification back", e.toString());
			}
		}

	}

	public void showmessage() {
		if (Constant.CASE_ID != null && Constant.CASE_ID.equals("")) {
			int size = prefs.getInt(Constant.SIZE, 0);
			if (size > 0) {
				Toast.makeText(
						getBaseContext(),
						getResources().getString(
								R.string.please_select_your_case),
						Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(getBaseContext(),
						getResources().getString(R.string.no_cases_available),
						Toast.LENGTH_SHORT).show();
			}

		} else {
			showMessageDialog();
		}

	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
	}

	private void updateListView() {
		updatecasenum();
		adapter.setData(casenum);
		Utility.setListViewHeightBasedOnChildren(lv_case);
		int size = prefs.getInt(Constant.SIZE, -1);
		if (size > 1) {
			if (!refreshstage.equals("") && !refreshstage.equals("null")) {
				int pos = findpositionofstage(refreshstage);
				loadimageswithanimation(pos);
				refreshstage = "";

			} else if (refreshstage.equals("") && iscasedeleted) {
				tv_case.setText(getResources().getString(R.string.select_case));
				int pos = 6;
				loadimageswithanimation(pos);
				iscasedeleted = false;
				Constant.position = -1;
				adapter.notifyDataSetChanged();
				lv_case.invalidate();
				Constant.CASE_ID = "";
			} else if (refreshstage.equals("null")) {
				loadimageswithanimation(6);
			} else if (refreshstage.equals("")) {
				loadimageswithanimation(6);
			}
		}
	}

	public void jsonResponse1(String result) {
		// Log.e("GCM RESULT", result);
		String res = "";
		JSONObject jobj = null;
		try {
			jobj = new JSONObject(result);
			JSONObject temp = jobj.optJSONObject(Constant.RESPONSE);
			res = temp.optString(Constant.STATUS);
			if (res != null)
				if (res.equalsIgnoreCase("TRUE")) {
					editor.putString(Constant.ISDEVICETOKEN,
							Constant.REGISTRATIONID);
					editor.apply();

				} else {

				}
		} catch (Exception e) {

		}

	}

	public void jsonResponse3(String result) {
		String res = "";
		JSONObject jobj = null;
		try {
			jobj = new JSONObject(result);
			JSONObject temp = jobj.getJSONObject(Constant.RESPONSE);
			res = temp.getString(Constant.STATUS);

		} catch (Exception e) {

		}
		if (res.equalsIgnoreCase("TRUE")) {
			SharedPreferences loginprefs = getSharedPreferences(
					Constant.LOGINPREF, MODE_PRIVATE);
			Editor editor1 = loginprefs.edit();
			editor1.clear().commit();
			Constant.position = -1;
			refreshstage = "";
			casedeleted = false;
			iscasedeleted = false;
			Constant.CASE_ID = "";

			SharedPreferences posprefs = getSharedPreferences(
					Constant.CASESHAREDPREF, MODE_PRIVATE);
			Editor editor2 = posprefs.edit();
			editor2.clear().commit();
			SharedPreferences col_timestamp_pref = getSharedPreferences(
					Constant.COLLECTIONTIMESTAMPSHAREDPREF, MODE_PRIVATE);
			Editor editor3 = col_timestamp_pref.edit();
			editor3.clear().commit();
			dbHelper.deleteMessageDatabase(Act_Home.this);

			startActivity(new Intent(Act_Home.this, Act_Login.class));
			Toast.makeText(getBaseContext(),
					getResources().getString(R.string.logout_success),
					Toast.LENGTH_SHORT).show();
			finish();
		} else if (res.equalsIgnoreCase("FALSE")) {

			try {
				String message = jobj.getString(Constant.MESSAGE);
				Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT)
						.show();
			} catch (Exception e) {

			}

		}

	}

	public void jsonResponse(String result) {
		JSONObject jsonResult = null;
		JSONArray jsonarray = null;
		JSONObject jsonresponse = null;
		editor.clear().commit();

		try {
			jsonResult = new JSONObject(result);
			jsonarray = jsonResult.optJSONArray(Constant.RESPONSE);
			String currentcasenum = "";
			String casenumber = "";
			refreshstage = "";

			boolean isdeleted = false;
			if (!tv_case.getText().toString().equals("")) {
				currentcasenum = tv_case.getText().toString();
			}
			if (jsonarray != null) {
				for (int i = 0; i < jsonarray.length(); i++) {
					JSONObject jobj = jsonarray.getJSONObject(i);

					casenumber = jobj.getString(Constant.CASENUMBERVALUE);
					if (!currentcasenum.equals("")
							&& !currentcasenum.equals(getResources().getString(
									R.string.select_case))
							&& casenumber.equals(currentcasenum)) {
						refreshstage = jobj.getString(Constant.STAGE);

					}

					editor.putString(Constant.ID + i, jobj.toString());

				}
				editor.putInt(Constant.SIZE, jsonarray.length());
				editor.commit();
			} else if (jsonarray == null) {

				jsonresponse = jsonResult.getJSONObject(Constant.RESPONSE);
				if (jsonresponse.has(Constant.RESPONSE)) {
					if (!jsonresponse.optString(Constant.RESPONSE).equals("")) {
						// JsonObject temp_obj=jsonresponse.getString("status");
						/*
						 * Toast.makeText( getBaseContext(),
						 * "Oops! The combination of Email address and code doesn't exist.Please try again!"
						 * , Toast.LENGTH_SHORT).show();
						 */
						JSONObject temp_obj = jsonresponse
								.getJSONObject(Constant.RESPONSE);
						String message = temp_obj.getString(Constant.MESSAGE);
						if (message
								.equalsIgnoreCase("Whoops! You do not appear to have any cases with us.")) {
							SharedPreferences posprefs = getSharedPreferences(
									Constant.CASESHAREDPREF, MODE_PRIVATE);
							Editor editor1 = posprefs.edit();
							editor1.clear().commit();

							editor.putInt(Constant.SIZE, 0);
							editor.commit();
						}
					}
				}

			}
			if (!currentcasenum.equals("")
					&& !currentcasenum.equals(getResources().getString(
							R.string.select_case)) && refreshstage.equals("")) {
				isdeleted = true;
				iscasedeleted = true;
				Constant.CASE_ID = "";
				tv_case.setText("");
				SharedPreferences posprefs = getSharedPreferences(
						Constant.CASESHAREDPREF, MODE_PRIVATE);
				Editor editor1 = posprefs.edit();
				editor1.clear().commit();
			}
			/*
			 * if (isdeleted) {
			 * tv_case.setText(getResources().getString(R.string.select_case));
			 * Constant.CASE_ID="";
			 * 
			 * }
			 */

			updateListView();

		} catch (Exception e) {

		}
	}

	private void callCaseTrackerApi() {
		try {

			new Async_Task(this, true).execute(Constant.EMAIL_ID,
					Constant.USER_UNIQUE_ID, Act_Home.this, "aftercall");
		} catch (Exception e) {
			Log.e("Exception for Login", e.toString());
		}

	}

	private void callLangChange() {
		String langid = prefs.getString(Constant.lang_id, Constant.eng_lang);
		if (!Constant.lang.equals(langid)) {
			try {

				new Async_Task(this, true).execute(Constant.EMAIL_ID,
						Constant.USER_UNIQUE_ID, Act_Home.this, "updateLang");
			} catch (Exception e) {
				Log.e("Exception for updateLogin", e.toString());
			}
		}

	}

	public void jsonResponse2(String result) {
		JSONObject jsonResult = null, jsonresponse;
		JSONArray jsonarray = null;

		try {
			jsonResult = new JSONObject(result);
			jsonarray = jsonResult.optJSONArray(Constant.RESPONSE);

			/*
			 * if (jsonarray != null && jsonarray.length() == 0) {
			 * Toast.makeText( getBaseContext(),
			 * "Oops! The combination of Email address and code doesn't exist.Please try again!"
			 * , Toast.LENGTH_SHORT).show(); return;
			 * 
			 * }
			 */if (jsonarray == null) {
				jsonresponse = jsonResult.getJSONObject(Constant.RESPONSE);
				if (jsonresponse.has(Constant.RESPONSE)) {
					if (!jsonresponse.optString(Constant.RESPONSE).equals("")) {
						// JsonObject temp_obj=jsonresponse.getString("status");
						/*
						 * Toast.makeText( getBaseContext(),
						 * "Oops! The combination of Email address and code doesn't exist.Please try again!"
						 * , Toast.LENGTH_SHORT).show();
						 */
						JSONObject temp_obj = jsonresponse
								.getJSONObject(Constant.RESPONSE);
						String message = temp_obj.getString(Constant.MESSAGE);
						if (message
								.equalsIgnoreCase("Whoops! Those details do not appear to be correct. Please check your details and try again.")) {
							Toast.makeText(getBaseContext(), message,
									Toast.LENGTH_SHORT).show();
							return;
						} else if (message
								.equalsIgnoreCase("Whoops! You do not appear to have any cases with us.")) {
							editor.putInt(Constant.SIZE, 0);
							editor.commit();
							initCaseTrackerValues();
						}

					}
				}
			}

			if (jsonarray != null) {
				for (int i = 0; i < jsonarray.length(); i++) {
					JSONObject jobj = jsonarray.getJSONObject(i);
					String errorcode = jobj.optString(Constant.ERRORCODE);
					if (errorcode != null && !errorcode.equals("")) {
						String message = jobj.optString(Constant.MESSAGE);
						Toast.makeText(getBaseContext(), message,
								Toast.LENGTH_SHORT).show();
						return;
					}

					editor.putString(Constant.ID + i, jobj.toString());

				}
				editor.putInt(Constant.SIZE, jsonarray.length());
				editor.commit();
				initCaseTrackerValues();
			}

		} catch (Exception e) {
		}

	}

	private void callInitialCaseTrackerApi() {
		try {

			new Async_Task(this, true).execute(Constant.EMAIL_ID,
					Constant.USER_UNIQUE_ID, Act_Home.this, "initialcall");
		} catch (Exception e) {
			Log.e("Exception for Login", e.toString());
		}

	}

	@Override
	protected void onPause() {
		super.onPause();

	}

	private void updatecasenum() {
		casenum = new ArrayList<String>();
		int size = prefs.getInt(Constant.SIZE, -1);

		if (size == 0) {
			SharedPreferences posprefs = getSharedPreferences(
					Constant.CASESHAREDPREF, MODE_PRIVATE);
			Editor editor1 = posprefs.edit();
			editor1.clear().commit();
			rel_sel_case.setEnabled(false);
			iv_select_Case.setVisibility(View.GONE);
			rel_sel_case.setVisibility(View.GONE);
			Constant.CASE_ID = "";
			iv_preparing.setVisibility(View.GONE);
			iv_pending.setVisibility(View.GONE);

			iv_awaiting.setVisibility(View.GONE);

			iv_allocated.setVisibility(View.GONE);

			iv_decision.setVisibility(View.GONE);
			iv_background.setVisibility(View.GONE);
			iv_preparing.setBackgroundResource(0);
			iv_pending.setBackgroundResource(0);
			iv_awaiting.setBackgroundResource(0);
			iv_allocated.setBackgroundResource(0);
			iv_decision.setBackgroundResource(0);
			iv_background.setBackgroundResource(0);

			rel_root_stages.setBackgroundResource(R.drawable.static_round);
			// makevisibilitygone();

		}

		else if (size > 1) {
			rel_sel_case.setVisibility(View.VISIBLE);
			iv_select_Case.setVisibility(View.VISIBLE);
			rel_sel_case.setEnabled(true);
			rel_root_stages.setBackgroundResource(0);

			for (int i = 0; i < size; i++) {
				try {
					JSONObject jobj = new JSONObject(prefs.getString(
							Constant.ID + i, ""));
					String casenumber = jobj
							.getString(Constant.CASENUMBERVALUE);
					casenum.add(casenumber);
				} catch (Exception e) {

				}
			}
			adapter.setData(casenum);

			adapter.notifyDataSetChanged();
			Utility.setListViewHeightBasedOnChildren(lv_case);

		} else if (size == 1) {
			try {
				rel_root_stages.setBackgroundResource(0);
				int pos;
				SharedPreferences posprefs = getSharedPreferences(
						Constant.CASESHAREDPREF, MODE_PRIVATE);
				Editor editor1 = posprefs.edit();
				editor1.clear().commit();
				JSONObject jobj = new JSONObject(prefs.getString(
						Constant.ID + 0, ""));
				String casenumber = jobj.getString(Constant.CASENUMBERVALUE);
				casenum.add(casenumber);
				tv_case.setText(casenumber);
				SharedPreferences posprefs1 = getSharedPreferences(
						Constant.CASESHAREDPREF, MODE_PRIVATE);
				Editor editor = posprefs1.edit();
				editor.putString(Constant.CASESTRING, casenumber);
				editor.commit();
				rel_sel_case.setEnabled(false);
				rel_sel_case.setVisibility(View.VISIBLE);
				iv_select_Case.setVisibility(View.GONE);
				iv_preparing.setEnabled(true);
				iv_allocated.setEnabled(true);
				iv_awaiting.setEnabled(true);
				iv_decision.setEnabled(true);
				iv_pending.setEnabled(true);
				iv_message.setEnabled(true);
				iv_checklist.setEnabled(true);
				iv_forum.setEnabled(true);
				iv_resources.setEnabled(true);

				Constant.position = 0;

				String stage = jobj.getString(Constant.STAGE);
				if (!stage.equalsIgnoreCase("null"))
					pos = findpositionofstage(stage);
				else
					pos = 6;
				String id = jobj.getString(Constant.ID);
				Constant.CASE_ID = id;
				loadimageswithanimation(pos);
				iv_select_Case.setVisibility(View.INVISIBLE);

			} catch (Exception e) {

			}
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		iv_forum.setEnabled(true);
		//iv_contactus.setBackgroundResource(R.drawable.contact_us);
		//iv_news.setBackgroundResource(R.drawable.news);
		//iv_blog.setBackgroundResource(R.drawable.blog_updated);

		if (Act_CheckList.isFromCheckList) {
			Act_CheckList.isFromCheckList = false;
			updatecasenum();
			adapter.setData(casenum);
			Utility.setListViewHeightBasedOnChildren(lv_case);

			int size = prefs.getInt(Constant.SIZE, -1);
			if (size > 1 && message_notification == null) {
				if (!refreshstage.equals("") && !refreshstage.equals("null")) {
					int pos = findpositionofstage(refreshstage);
					loadimageswithanimation(pos);
					refreshstage = "";

				} else if (refreshstage.equals("null")) {
					// tv_case.setText(getResources().getString(R.string.select_case));
					loadimageswithanimation(6);
					refreshstage = "";

				} else if (refreshstage.equals("") && casedeleted) {

					tv_case.setText(getResources().getString(
							R.string.select_case));
					casedeleted = false;
					loadimageswithanimation(6);

				}
			}
		} else if (Act_CheckList.isFromNotification.equalsIgnoreCase("true")) {

			Act_CheckList.isFromNotification = "false";
			onNotificationUpdae();
		} else if (Act_Message.isFromNotificationMessage.equals("true")) {
			Act_Message.isFromNotificationMessage = "false";
			SharedPreferences posprefs = getSharedPreferences(
					Constant.CASESHAREDPREF, MODE_PRIVATE);
			Editor editor = posprefs.edit();
			editor.putString(Constant.CASESTRING, Constant.NOTIFICATION_CASE_ID);
			editor.commit();

			Constant.ISFROMNOTIFICATION = false;
			Constant.NOTIFICATION_CASE_ID = "";
			onNotificationUpdae();
		} else if (Act_Message.isFromMessageBack) {

			Act_Message.isFromMessageBack = false;
			if (Act_Message.notificationflag == false) {
				Log.e("Message Back Screen", "Message Back");
				SharedPreferences posprefs = getSharedPreferences(
						Constant.CASESHAREDPREF, MODE_PRIVATE);
				Editor editor = posprefs.edit();
				editor.putString(Constant.CASESTRING,
						Constant.NOTIFICATION_CASE_ID);
				editor.commit();

				Constant.ISFROMNOTIFICATION = false;
				Constant.NOTIFICATION_CASE_ID = "";
				onNotificationUpdae();
				Act_Message.notificationflag = true;
			}

		} else if (Constant.ISFROMNOTIFICATION) {
			home_noti_flag = false;
			SharedPreferences posprefs = getSharedPreferences(
					Constant.CASESHAREDPREF, MODE_PRIVATE);
			Editor editor = posprefs.edit();
			editor.putString(Constant.CASESTRING, Constant.NOTIFICATION_CASE_ID);
			editor.commit();

			Constant.ISFROMNOTIFICATION = false;
			Constant.NOTIFICATION_CASE_ID = "";
			onNotificationUpdae();
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);
		setIntent(intent);
		// home_noti_flag=true;
		// Log.e("Home notification","In Home Notification new intent");

	}

	public void jsonResonseForUpdateLang(String result) {
		// TODO Auto-generated method stub
		try {

			JSONObject object = new JSONObject(result);
			JSONObject response = object.optJSONObject(Constant.RESPONSE);
			String status = response.optString("status");
			if (status.equalsIgnoreCase("Success")) {
				editor.putString(Constant.lang_id, Constant.lang);
				editor.apply();

			} else {
				iv_flag.performClick();
			}
			if (response.has("message")) {
				Toast.makeText(getApplicationContext(),
						response.optString("message"), Toast.LENGTH_LONG)
						.show();
			}

		} catch (Exception e) {
			e.printStackTrace();
			iv_flag.performClick();
			/*
			 * System.out.println("Please try again");
			 */
		}
	}
}