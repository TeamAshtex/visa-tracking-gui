package com.globalenterprisesgroup;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.crittercism.app.Crittercism;
import com.globalenterprice.helper.Constant;

public class Act_Splash extends Activity {

	// Splash screen timer
	private static int SPLASH_TIME_OUT = 1000;
	SharedPreferences loginprefs, helpscreensplash,prefs;
	boolean isHelpCalled;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		Crittercism.initialize(getApplicationContext(), getResources()
				.getString(R.string.CRITTERCISM_APP_ID));

		// Remove notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.act_spash);

		new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

			@Override
			public void run() {
				// This method will be executed once the timer is over
				// Start your app main activity
				loginprefs = getSharedPreferences(Constant.LOGINPREF,
						MODE_PRIVATE);
				helpscreensplash = getSharedPreferences(
						Constant.HELPSHAREDPREF, MODE_PRIVATE);
				prefs = getSharedPreferences(Constant.SHARED_PREF, MODE_PRIVATE);

				isHelpCalled = helpscreensplash.getBoolean(
						Constant.ISHELPCALLED, false);
				Constant.lang = prefs.getString(Constant.lang_id, Constant.eng_lang);

				// isHelpCalled=false;
				if (isHelpCalled) {

					String email = loginprefs.getString(
							Constant.LOGINUSEREMAIL, "");
					String password = loginprefs.getString(Constant.LOGINPASS,
							"");
					if (email.equalsIgnoreCase("")) {
						startActivity(new Intent(Act_Splash.this,
								Act_Login.class));
					} else {
						Constant.EMAIL_ID = email;
						Constant.USER_UNIQUE_ID = password;
						startActivity(new Intent(Act_Splash.this,
								Act_Home.class));
					}
				} else {
					startActivity(new Intent(Act_Splash.this, Act_Help.class));
				}

				finish();
			}
		}, SPLASH_TIME_OUT);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

	}

}
