package com.globalenterprisesgroup;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.globalenterprice.helper.Act_ConnectionDetector;
import com.globalenterprice.helper.Async_Task;
import com.globalenterprice.helper.CheckListAdapter;
import com.globalenterprice.helper.Constant;
import com.globalenterprisesgroup.R;

public class Act_CheckList extends Activity {

	private ArrayList<String> checkedItemsDesc = new ArrayList<String>();
	private ArrayList<String> checkedItemNotes = new ArrayList<String>();

	private ArrayList<Boolean> checkedItem = new ArrayList<Boolean>();
	SharedPreferences prefs;
	ListView lv_checkList;
	private TextView tv_title, tv_checkmessage;
	private ImageView iv_back, iv_refresh;
	private String check_id, casetext = "";
	private boolean ischecklisdeleted = false;
	public static boolean isFromCheckList = false;
	public static String isFromNotification = "false";
	public static Act_CheckList act_CheckList;

	Editor editor;
	JSONObject checklist_object;
	CheckListAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.act_checklist);
		init();

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		initCheckListData();
		setAdapter();
	}

	private void callCaseTrackerApi() {
		try {

			new Async_Task(this, true).execute(Constant.EMAIL_ID,
					Constant.USER_UNIQUE_ID, Act_CheckList.this);
		} catch (Exception e) {
			Log.e("Exception for Login", e.toString());
		}

	}

	public void jsonResponse(String result) {
		JSONObject jsonResult = null;
		JSONObject jsonresponse = null;
		JSONArray jsonarray = null;
		editor.clear().commit();
		checklist_object = null;

		try {
			jsonResult = new JSONObject(result);
			isFromCheckList = true;
			jsonarray = jsonResult.optJSONArray(Constant.RESPONSE);
			String stage = "";
			if (jsonarray != null) {
				for (int i = 0; i < jsonarray.length(); i++) {
					JSONObject jobj = jsonarray.getJSONObject(i);
					String id = jobj.getString(Constant.ID);
					if (id.equalsIgnoreCase(check_id)) {
						checklist_object = jobj;
						stage = jobj.getString(Constant.STAGE);
						Act_Home.refreshstage = stage;

					}

					editor.putString(Constant.ID + i, jobj.toString());

				}
				editor.putInt(Constant.SIZE, jsonarray.length());
				editor.commit();

			} else if (jsonarray == null) {

				jsonresponse = jsonResult.getJSONObject(Constant.RESPONSE);
				if (jsonresponse.has(Constant.RESPONSE)) {
					if (!jsonresponse.optString(Constant.RESPONSE).equals("")) {
						// JsonObject temp_obj=jsonresponse.getString("status");
						/*
						 * Toast.makeText( getBaseContext(),
						 * "Oops! The combination of Email address and code doesn't exist.Please try again!"
						 * , Toast.LENGTH_SHORT).show();
						 */
						JSONObject temp_obj = jsonresponse
								.getJSONObject(Constant.RESPONSE);
						String message = temp_obj.getString(Constant.MESSAGE);
						if (message
								.equalsIgnoreCase("Whoops! You do not appear to have any cases with us.")) {
							SharedPreferences posprefs = getSharedPreferences(
									Constant.CASESHAREDPREF, MODE_PRIVATE);
							Editor editor1 = posprefs.edit();
							editor1.clear().commit();

							editor.putInt(Constant.SIZE, 0);
							editor.commit();
						}
					}
				}

			}
			if (checklist_object == null) {
				Act_Home.refreshstage = "";
				Act_Home.casedeleted = true;
				SharedPreferences posprefs = getSharedPreferences(
						Constant.CASESHAREDPREF, MODE_PRIVATE);
				Editor editor1 = posprefs.edit();
				editor1.clear().commit();

			}
			if (checklist_object != null && stage == "null") {
				Act_Home.refreshstage = "null";
			}
			updateListView();

		} catch (Exception e) {
			Log.e("Login Exception", "Login Response " + e);
		}
	}

	private void updateListView() {
		try {
			checkedItemsDesc.clear();
			checkedItem.clear();
			checkedItemNotes.clear();
			if (checklist_object != null) {
				JSONArray children = checklist_object
						.getJSONArray(Constant.CHILDREN);

				if (children.length() == 0) {
					adapter.notifyDataSetChanged();
					showAlertDialog1();
				} else {
					for (int i = 0; i < children.length(); i++) {
						JSONObject jobj_children = (JSONObject) children.get(i);
						String desc = jobj_children
								.getString(Constant.CHECKLIST);
						String notes = jobj_children.getString(Constant.NOTES);
						boolean status = Boolean.parseBoolean(jobj_children
								.getString(Constant.CHECKED));
						checkedItemsDesc.add(desc);
						checkedItem.add(status);
						checkedItemNotes.add(notes);
					}
					adapter.notifyDataSetChanged();

				}
			} else {
				adapter.notifyDataSetChanged();
				Toast.makeText(getBaseContext(),
						"Your current case tracker is removed.",
						Toast.LENGTH_SHORT).show();
				Constant.CASE_ID = "";

			}
		} catch (Exception e) {

		}
	}

	private void init() {
		act_CheckList = this;
		prefs = getSharedPreferences(Constant.SHARED_PREF, MODE_PRIVATE);
		editor = prefs.edit();
		isFromNotification = getIntent().getStringExtra("messagenotification");
		if (isFromNotification == null)
			isFromNotification = "false";
		tv_checkmessage = (TextView) findViewById(R.id.tv_check_message);
		tv_checkmessage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				DialogInterface.OnClickListener onClick = new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						if (which == DialogInterface.BUTTON_POSITIVE) {
							Intent messageactivity = new Intent(
									Act_CheckList.this, Act_Message.class);

							messageactivity.putExtra(Constant.MESSAGECASNUMBER,
									casetext);
							startActivity(messageactivity);
						} else if (which == DialogInterface.BUTTON_NEGATIVE) {
							dialog.dismiss();
						}
					}

				};
				Builder builder = new AlertDialog.Builder(Act_CheckList.this);

				builder.setMessage(getResources().getString(
						R.string.dialog_message));

				builder.setCancelable(true);
				builder.setPositiveButton("Yes", onClick);
				builder.setNegativeButton("No", onClick);

				AlertDialog dialog = builder.create();

				dialog.show();

			}
		});
		iv_refresh = (ImageView) findViewById(R.id.iv_refresh);
		iv_refresh.setVisibility(View.VISIBLE);
		iv_refresh.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Act_ConnectionDetector _conn = new Act_ConnectionDetector(
						Act_CheckList.this);
				if (_conn.isConnectingToInternet()) {

					callCaseTrackerApi();
				}

				else {
					Toast.makeText(getBaseContext(),
							getResources().getString(R.string.no_internet),
							Toast.LENGTH_SHORT).show();
				}

			}
		});
		tv_title = (TextView) findViewById(R.id.tv_title);
		tv_title.setText(getResources().getString(R.string.checklist));
		iv_back = (ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				finish();
			}
		});
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		act_CheckList = null;
	}

	private void initCheckListData() {
		if (Constant.ISFROMNOTIFICATION) {

			SharedPreferences posprefs = getSharedPreferences(
					Constant.CASESHAREDPREF, MODE_PRIVATE);
			Editor editor = posprefs.edit();
			editor.putString(Constant.CASESTRING, Constant.NOTIFICATION_CASE_ID);
			editor.commit();
			Constant.NOTIFICATION_CASE_ID = "";
		}
		checkedItemsDesc.clear();
		checkedItem.clear();
		prefs = getSharedPreferences(Constant.SHARED_PREF, MODE_PRIVATE);
		int size = prefs.getInt(Constant.SIZE, 0);
		for (int i = 0; i < size; i++) {
			try {
				JSONObject jobj = new JSONObject(prefs.getString(Constant.ID
						+ i, ""));
				String caseid = jobj.getString(Constant.ID);

				if (caseid.equalsIgnoreCase(Constant.CASE_ID)) {
					Constant.CASE_ID = caseid;
					casetext = jobj.getString(Constant.CASENUMBERVALUE);
					JSONArray children = jobj.getJSONArray(Constant.CHILDREN);
					check_id = jobj.getString(Constant.ID);
					if (children.length() == 0) {

						showAlertDialog1();

					} else {
						for (int j = 0; j < children.length(); j++) {
							JSONObject jobj_children = (JSONObject) children
									.get(j);
							String desc = jobj_children
									.getString(Constant.CHECKLIST);
							boolean status = Boolean.parseBoolean(jobj_children
									.getString(Constant.CHECKED));
							String notes = jobj_children.getString(Constant.NOTES);
							checkedItemsDesc.add(desc);
							checkedItemNotes.add(notes);
							checkedItem.add(status);
						}
					}

				}
			} catch (Exception e) {

			}

		}
	}

	/*
	 * try { JSONObject jobj = new JSONObject(prefs.getString(Constant.ID +
	 * Constant.position, "")); } catch (Exception e) {
	 * 
	 * }
	 */

	private void showAlertDialog1() {
		// title.setTextColor(getResources().getColor(R.color.greenBG));

		DialogInterface.OnClickListener onClick = new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				if (which == DialogInterface.BUTTON_POSITIVE) {
					dialog.dismiss();
				}
			}

		};

		Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getResources().getString(R.string.checklist));
		builder.setMessage(getResources().getString(
				R.string.no_checklist_under_this_casetracker));

		builder.setCancelable(true);
		builder.setPositiveButton(getResources().getString(R.string.ok),
				onClick);

		AlertDialog dialog = builder.create();

		dialog.show();
	}

	/*
	 * private void showAlertDialog() { AlertDialog.Builder builder = new
	 * AlertDialog.Builder( Act_CheckList.this); TextView title = new
	 * TextView(this); title.setText("CheckList"); title.setPadding(10, 10, 10,
	 * 10); title.setGravity(Gravity.CENTER); //
	 * title.setTextColor(getResources().getColor(R.color.greenBG));
	 * title.setTextAppearance(this, android.R.style.TextAppearance_Medium);
	 * builder.setCustomTitle(title);
	 * 
	 * builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	 * 
	 * @Override public void onClick(DialogInterface dialog, int which) { //
	 * TODO Auto-generated method stub dialog.dismiss(); } }); AlertDialog
	 * dialog1 = builder.create(); TextView messageText = (TextView) dialog1
	 * .findViewById(android.R.id.message); messageText.setTextAppearance(this,
	 * android.R.style.TextAppearance_Small);
	 * messageText.setText("No checklist under this CasteTracker");
	 * messageText.setGravity(Gravity.CENTER); dialog1.show();
	 * 
	 * }
	 */
	private void setAdapter() {
		lv_checkList = (ListView) findViewById(R.id.lv_check_list);

		adapter = new CheckListAdapter(Act_CheckList.this, checkedItemsDesc,
				checkedItem,checkedItemNotes);
		lv_checkList.setAdapter(adapter);

	}

}