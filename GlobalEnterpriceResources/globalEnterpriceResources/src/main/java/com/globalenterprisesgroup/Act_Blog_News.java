package com.globalenterprisesgroup;

import java.util.List;
import org.json.JSONObject;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.globalenterprice.helper.Act_ConnectionDetector;
import com.globalenterprice.helper.Constant;
import com.globalenterprice.rssparsing.BlogNewsAdapter;
import com.globalenterprice.rssparsing.RssFeedStructure;
import com.globalenterprice.rssparsing.XMLHandler;
import com.globalenterprisesgroup.R;

public class Act_Blog_News extends Activity {
	private static String rsslink = "", title;
	ListView newsList;
	List<JSONObject> news;
	List<RssFeedStructure> rssStr;
	private BlogNewsAdapter _adapter;
	private TextView tv_title;
	private ImageView iv_back;
	Act_ConnectionDetector _conn_detector;
	private String isfrom;
	ImageView iv_refrsh;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.act_blog);
		init();

		newsList = (ListView) findViewById(R.id.lv_blog_news);
		if (_conn_detector.isConnectingToInternet()) {
			RssFeedTask rssTask = new RssFeedTask();
			rssTask.execute();
		} else {
			Toast.makeText(getBaseContext(),
					getResources().getString(R.string.no_internet),
					Toast.LENGTH_SHORT).show();
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (_adapter != null) {
			_adapter.setOnResume(true);
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (_adapter != null) {
			_adapter.setOnResume(false);
		}
	}

	private void init() {
		iv_refrsh = (ImageView) findViewById(R.id.iv_refresh);
		iv_refrsh.setVisibility(View.VISIBLE);
		iv_refrsh.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (_conn_detector.isConnectingToInternet()) {
					RssFeedTask rssTask = new RssFeedTask();
					rssTask.execute();
				} else {
					Toast.makeText(getBaseContext(),
							getResources().getString(R.string.no_internet),
							Toast.LENGTH_SHORT).show();
				}
			}
		});
		isfrom = getIntent().getStringExtra(Constant.ISFROM);
		_conn_detector = new Act_ConnectionDetector(Act_Blog_News.this);
		iv_back = (ImageView) findViewById(R.id.iv_back);
		tv_title = (TextView) findViewById(R.id.tv_title);
		rsslink = getIntent().getStringExtra(Constant.URL);
		title = getIntent().getStringExtra(Constant.TITLE);
		tv_title.setText(title);
		iv_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Act_CheckList.isFromCheckList = false;
				finish();
			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Act_CheckList.isFromCheckList = false;
	}

	private class RssFeedTask extends AsyncTask<String, Void, String> {
		private ProgressDialog dialog;
		String response;

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub

			if (rssStr != null) {
				if (_adapter == null) {
					_adapter = new BlogNewsAdapter(Act_Blog_News.this, rssStr,
							isfrom);
					newsList.setAdapter(_adapter);
				} else {
					_adapter.notifyDataSetChanged();
				}
			}

			dialog.dismiss();
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			dialog = new ProgressDialog(Act_Blog_News.this);
			dialog.setMessage(getResources().getString(R.string.loading));
			dialog.show();
			dialog.setCancelable(false);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			try {
				String feed = rsslink;
				XMLHandler handler = new XMLHandler();
				rssStr = handler.getLatestArticles(feed);
			} catch (Exception e) {
				e.printStackTrace();
			}

			return response;
		}

	}

}
