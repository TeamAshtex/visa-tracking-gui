package com.globalenterprisesgroup;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.globalenterprice.helper.Constant;

public class Act_WebView extends Activity {
	WebView wv;
	String url, title;
	TextView tv_title;
	ProgressBar Pbar;
	ImageView iv_back;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.act_webview);

		init();
		url = getIntent().getStringExtra(Constant.URL);
		wv = (WebView) findViewById(R.id.webview_contactus);
		WebSettings settings = wv.getSettings();
		settings.setLoadWithOverviewMode(true);
		settings.setUseWideViewPort(true);
		wv.getSettings().setJavaScriptEnabled(true);

		wv.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		wv.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				if (progress < 100 && Pbar.getVisibility() == ProgressBar.GONE) {
					Pbar.setVisibility(ProgressBar.VISIBLE);

				}
				Pbar.setProgress(progress);
				if (progress == 100) {
					Pbar.setVisibility(ProgressBar.GONE);

				}
			}
		});

		wv.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				if (url != null && url.startsWith("market://")) {
					view.getContext().startActivity(
							new Intent(Intent.ACTION_VIEW, Uri.parse(url)));

				} else {
					view.loadUrl(url);

				}

				return true;
			}

			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				if (failingUrl.equals(url)) {
					Toast.makeText(
							getBaseContext(),
							getResources().getString(
									R.string.failed_to_load_url),
							Toast.LENGTH_SHORT).show();
				}

			}

			public void onReceivedSslError(WebView view,
					final SslErrorHandler handler, SslError error) {

				AlertDialog.Builder builder = new AlertDialog.Builder(
						Act_WebView.this);
				builder.setMessage(R.string.notification_error_ssl_cert_invalid);
				builder.setPositiveButton(
						getResources().getString(R.string.continue_txt),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								handler.proceed();
							}
						});
				builder.setNegativeButton(
						getResources().getString(R.string.cancel),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								handler.cancel();
								finish();
							}
						});
				final AlertDialog dialog = builder.create();
				dialog.show();
			}

		});
		wv.loadUrl(url);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (wv != null)
			wv.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (wv != null)
			wv.onPause();
	}

	private void init() {
		Pbar = (ProgressBar) findViewById(R.id.pB1);
		tv_title = (TextView) findViewById(R.id.tv_title);
		title = getIntent().getStringExtra(Constant.TITLE);
		tv_title.setText(title);
		iv_back = (ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Act_CheckList.isFromCheckList = false;
				finish();
			}
		});
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Act_CheckList.isFromCheckList = false;
	}
	/*
	 * private class SSLTolerentWebViewClient extends WebViewClient {
	 * 
	 * @Override public void onReceivedSslError(WebView view, SslErrorHandler
	 * handler, SslError error) { handler.proceed(); // Ignore SSL certificate
	 * errors }
	 * 
	 * }
	 */
}
