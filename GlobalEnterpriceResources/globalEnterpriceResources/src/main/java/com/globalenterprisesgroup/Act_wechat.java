package com.globalenterprisesgroup;

import java.io.File;
import java.io.FileOutputStream;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.globalenterprice.helper.Act_ConnectionDetector;
import com.globalenterprice.helper.Constant;

public class Act_wechat extends Activity implements OnClickListener {
	Act_ConnectionDetector _conn_detector;
	ImageView iv_back, img_barcode;
	TextView tv_title, tv_save_qr;
	Button btn_save_gallary;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.act_wechat);
		init();
		setListner();
	}

	private void init() {
		_conn_detector = new Act_ConnectionDetector(Act_wechat.this);

		iv_back = (ImageView) findViewById(R.id.iv_back);
		img_barcode = (ImageView) findViewById(R.id.img_barcode);
		tv_title = (TextView) findViewById(R.id.tv_title);
		tv_title.setText(getResources().getString(R.string.wechat));
		tv_save_qr = (TextView) findViewById(R.id.tv_save_qr);
		btn_save_gallary = (Button) findViewById(R.id.btn_save_gallary);
	}

	private void setListner() {
		btn_save_gallary.setOnClickListener(this);
		iv_back.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_save_gallary:
			SaveImage();

			break;
		case R.id.iv_back:
			finish();
			break;
		default:
			break;
		}
	}

	private void SaveImage() {
		try {
			Bitmap bm = BitmapFactory.decodeResource(getResources(),
					R.drawable.qrcode);
			String extStorageDirectory = Environment
					.getExternalStorageDirectory().toString();
			File file = new File(extStorageDirectory, "qrcode.PNG");
			if (!file.exists()) {
				FileOutputStream outStream = new FileOutputStream(file);
				bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
				outStream.flush();
				outStream.close();
			}
			Intent intent = new Intent(Act_wechat.this, Act_WebView.class);
			intent.putExtra(Constant.URL, Constant.WECHATURL);
			intent.putExtra(Constant.TITLE,
					getResources().getString(R.string.wechat));
			callIntent(intent);
		} catch (Exception e) {

			System.out.println("Error=" + e.getMessage());
		}

	}

	private void callIntent(Intent intent) {
		if (_conn_detector.isConnectingToInternet())

			startActivity(intent);
		else
			Toast.makeText(getBaseContext(),
					getResources().getString(R.string.no_internet),
					Toast.LENGTH_SHORT).show();
	}
}
