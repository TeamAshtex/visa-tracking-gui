package com.globalenterprisesgroup;

import static com.cloudNotificationGCM.CommonUtilities.SENDER_ID;
import static com.cloudNotificationGCM.CommonUtilities.displayMessage;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.cloudNotificationGCM.ServerUtilities;
import com.globalenterprice.helper.Constant;
import com.globalenterprice.helper.ServiceHandler;
import com.globalenterprisesgroup.R;
import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;

/**
 * IntentService responsible for handling GCM messages.
 */
public class GCMIntentService extends GCMBaseIntentService {

	@SuppressWarnings("hiding")
	private static final String TAG = "GCMIntentService";
	static Editor editor;
	static SharedPreferences prefs;
	static NotificationManager notificationManager;

	protected static String localRegId;

	public GCMIntentService() {
		super(SENDER_ID);
		System.out.println("IN GCMIntentService");

	}

	@Override
	protected void onRegistered(final Context context, String registrationId) {
		//Log.i(TAG, "Device registered: regId = " + registrationId);

		displayMessage(context, getString(R.string.gcm_registered));

		/*
		 * new Thread(new Runnable() { public void run() { shareRegId =
		 * getSharedPreferences("RegistrationID", 1); String registrationId =
		 * shareRegId.getString("regID", "");
		 * System.out.println("MyApplcation "+MyApplication.registrationId);
		 * 
		 * String android_id =
		 * Secure.getString(context.getContentResolver(),Secure.ANDROID_ID);
		 * 
		 * 
		 * String response; //DefaultHttpClient hc = new DefaultHttpClient();
		 * //ResponseHandler<String> res = new BasicResponseHandler(); //HttpGet
		 * postMethod = new HttpGet(URL_Class.PUSH_NOTIFICATION_REGISTRATION);
		 * //List<NameValuePair> nameValuePairs = new
		 * ArrayList<NameValuePair>();
		 * 
		 * String userIdLocal;
		 * if(GetUserProperty.getUserId(getApplicationContext
		 * ()).equalsIgnoreCase("0")) { userIdLocal = "0"; } else { userIdLocal
		 * = GetUserProperty.getUserId(getApplicationContext()); }
		 * 
		 * 
		 * if(!registrationId.equalsIgnoreCase("")) { localRegId =
		 * registrationId; } else
		 * if(!MyApplication.registrationId.equalsIgnoreCase("")) { localRegId =
		 * MyApplication.registrationId; } else
		 * if(!SplashActivityFirst.regId.equalsIgnoreCase("")) { localRegId =
		 * SplashActivityFirst.regId; }
		 * 
		 * ParsingClass parse = new ParsingClass(getApplicationContext());
		 * String pushURL = URL_Class.PUSH_NOTIFICATION_REGISTRATION; pushURL=
		 * pushURL+URL_Class.USER_ID+userIdLocal+"&"; pushURL=
		 * pushURL+"token="+localRegId+"&"; pushURL=
		 * pushURL+"hardwareid="+android_id+"&"; pushURL=
		 * pushURL+"device_type="+"A";
		 * 
		 * 
		 * Element ele = parse.startUpConfigrationOfParsing(pushURL);
		 * //nameValuePairs.add(new
		 * BasicNameValuePair("hardwareid",android_id));
		 * //nameValuePairs.add(new BasicNameValuePair("device_type","A"));
		 * 
		 * try { postMethod.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		 * } catch (UnsupportedEncodingException e1) { e1.printStackTrace(); }
		 * 
		 * //response = hc.execute(postMethod, res);
		 * //System.out.println("Push Resp :"+ele); if(ele != null) { Editor
		 * editorId = shareRegId.edit(); if(localRegId != null &&
		 * !localRegId.equalsIgnoreCase("")) { editorId.putString("regID",
		 * localRegId); editorId.commit(); } } //FinalData =
		 * GETDATAFROM_XML(response);
		 * //System.out.println("Response Inside WebView_Data Method :"
		 * +response);
		 * 
		 * }
		 * 
		 * }).start();
		 */
		ServerUtilities.register(context, registrationId);
	}

	@Override
	protected void onUnregistered(Context context, String registrationId) {
		Log.i(TAG, "Device unregistered");
		displayMessage(context, getString(R.string.gcm_unregistered));
		if (GCMRegistrar.isRegisteredOnServer(context)) {
			// ServerUtilities.unregister(context, registrationId);
		} else {
			// This callback results from the call to unregister made on
			// ServerUtilities when the registration to the server failed.
			Log.i(TAG, "Ignoring unregister callback");
		}
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		Log.i(TAG, "Received message");

		// displayMessage(context, message);

		try {
			// System.out.println("Message recieved is "
			// + intent.getExtras().getString("data"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			// System.out.println("Message recieved is "
			// + intent.getExtras().getString("registration_ids"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			// System.out.println("Message recieved is "
			// + intent.getExtras().getString("id"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			// System.out.println("Message recieved is "
			// + intent.getExtras().getString("message"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			System.out.println("Message extras recieved is "
					+ intent.getExtras() + "");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// notifies user
		generateNotification(context, intent);
	}

	@Override
	protected void onDeletedMessages(Context context, int total) {
		Log.i(TAG, "Received deleted messages notification");
		// String message = getString(R.string.gcm_deleted, total);
		// displayMessage(context, message);
		// notifies user
		// generateNotification(context, intent);
	}

	@Override
	public void onError(Context context, String errorId) {
		Log.i(TAG, "Received error: " + errorId);
		displayMessage(context, "Received error:"+ errorId);
	}

	public static void callcasetrackerapi(Context context, String email,
			String pass) {
		prefs = context
				.getSharedPreferences(Constant.SHARED_PREF, MODE_PRIVATE);
		editor = prefs.edit();
		String url = Constant.LOGINAPI;
		ServiceHandler sh = new ServiceHandler();
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(Constant.EMAIL, email));
		params.add(new BasicNameValuePair(Constant.UNIQUENUMBER, pass));
		String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET, params);
		JSONObject jsonResult = null, jsonresponse;
		JSONArray jsonarray = null;

		try {
			jsonResult = new JSONObject(jsonStr);
			jsonarray = jsonResult.optJSONArray(Constant.RESPONSE);

			if (jsonarray == null) {
				jsonresponse = jsonResult.getJSONObject(Constant.RESPONSE);
				if (jsonresponse.has(Constant.RESPONSE)) {
					if (!jsonresponse.optString(Constant.RESPONSE).equals("")) {
						// JsonObject temp_obj=jsonresponse.getString("status");
						/*
						 * Toast.makeText( getBaseContext(),
						 * "Oops! The combination of Email address and code doesn't exist.Please try again!"
						 * , Toast.LENGTH_SHORT).show();
						 */
						JSONObject temp_obj = jsonresponse
								.getJSONObject(Constant.RESPONSE);
						String message = temp_obj.getString(Constant.MESSAGE);
						if (message
								.equalsIgnoreCase("Whoops! Those details do not appear to be correct. Please check your details and try again.")) {
							Toast.makeText(context, message, Toast.LENGTH_SHORT)
									.show();
							return;
						} else if (message
								.equalsIgnoreCase("Whoops! You do not appear to have any cases with us.")) {
							editor.putInt(Constant.SIZE, 0);
							editor.commit();
						}

					}
				}
			}

			if (jsonarray != null) {
				for (int i = 0; i < jsonarray.length(); i++) {
					JSONObject jobj = jsonarray.getJSONObject(i);
					String errorcode = jobj.optString(Constant.ERRORCODE);
					if (errorcode != null && !errorcode.equals("")) {
						String message = jobj.optString(Constant.MESSAGE);
						Toast.makeText(context, message, Toast.LENGTH_SHORT)
								.show();
						return;
					}

					editor.putString(Constant.ID + i, jobj.toString());

				}
				editor.putInt(Constant.SIZE, jsonarray.length());
				editor.commit();

			}

		} catch (Exception e) {
		}

	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		// log message
		Log.i(TAG, "Received recoverable error: " + errorId);
		displayMessage(context,
				"Received recoverable error:"+errorId.toString());
		return super.onRecoverableError(context, errorId);
	}

	/**
	 * Issues a notification to inform the user that server has sent a message.
	 */
	public static void generateNotification(Context context, Intent intent) {
		//JSONArray jarray = null;
		JSONObject jobj = null;
		String caseid = "", type = "";
		String notificationmessage = "";

		//String message = intent.getExtras().getString("message");
		Bundle bundle = intent.getExtras();

		String android = bundle.getString("android");

		try {

			jobj = new JSONObject(android);
		} catch (Exception e) {

		}
		int icon = R.drawable.app_icon;
		long when = System.currentTimeMillis();
		Intent notificationIntent = null;
		try {
			notificationmessage = jobj.getString("title");
			caseid = jobj.getString("caseid");
			type = jobj.getString("type");
		} catch (Exception e) {

		}

		if(notificationManager!=null)
			notificationManager.cancel(0);
		notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);

		Notification notification = new Notification(icon, notificationmessage,
				when);
		String title = context.getString(R.string.app_name);

		/*SharedPreferences posprefs = context.getSharedPreferences(
				Constant.CASESHAREDPREF, MODE_PRIVATE);
		Editor editor = posprefs.edit();
		editor.putString(Constant.CASESTRING, caseid);
		editor.commit();
*/
		Constant.NOTIFICATION_CASE_ID=caseid;
		
				
		SharedPreferences loginprefs = context.getSharedPreferences(
				Constant.LOGINPREF, MODE_PRIVATE);
		String email = loginprefs.getString(Constant.LOGINUSEREMAIL, "");

		if (!email.equals("")) {
			String password = loginprefs.getString(Constant.LOGINPASS, "");
			callcasetrackerapi(context, email, password);
			Constant.ISFROMNOTIFICATION=true;
			if (type.equalsIgnoreCase("1")) {
				notificationIntent = new Intent(context, Act_Home.class);
				try
				{
			//	boolean foregroud = new ForegroundCheckTask().execute(context).get();
				//if(foregroud)
				//{
					Constant.ISFROMNOTIFICATION=true;
				//}
				//else
				//{
				//	Constant.ISFROMNOTIFICATION=true;
				//}
				}
				catch
				(Exception e)
				{
					
				}
				

			} else if (type.equalsIgnoreCase("3")) {
				notificationIntent = new Intent(context, Act_Message.class);
				/*if(Act_Message.act_message!=null)
				{
					Act_Message.act_message.setnotificationflag(true);
					
				}
				*/
				/*if(Act_CheckList.act_CheckList!=null)
				{
					Act_CheckList.act_CheckList.finish();
				}*/
				

			} else if (type.equalsIgnoreCase("2")) {
				notificationIntent = new Intent(context, Act_CheckList.class);
				
				Act_CheckList.isFromNotification="true";
				/*if(Act_Message.act_message!=null)
				{
					Act_Message.act_message.finish();
				}*/
				int size = prefs.getInt(Constant.SIZE, 0);
				for (int i = 0; i < size; i++) {
					try {
						JSONObject jobj_checklist = new JSONObject(prefs.getString(
								Constant.ID + i, ""));
						
						String casenum = jobj_checklist
								.getString(Constant.CASENUMBERVALUE);
						if (casenum.equalsIgnoreCase(caseid)) {
							String checkcaseid = jobj_checklist.getString(Constant.ID);
							Constant.CASE_ID = checkcaseid;
						}
					} catch (Exception e) {

					}
				}

			}
		} else {
			notificationIntent = new Intent(context, Act_Login.class);
		}
		notificationIntent.putExtra("messagenotification", "true");
		
		notificationIntent.putExtra(Constant.MESSAGECASNUMBER, caseid);
		
		notification.defaults = Notification.DEFAULT_ALL;
		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent pendingintent = PendingIntent.getActivity(context, 0,
				notificationIntent,PendingIntent.FLAG_UPDATE_CURRENT);
	/*	notification.setLatestEventInfo(context, title, notificationmessage,
				pendingintent);
*/		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(0, notification);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

}
