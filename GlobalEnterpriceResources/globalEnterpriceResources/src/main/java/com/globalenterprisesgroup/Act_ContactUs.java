package com.globalenterprisesgroup;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.globalenterprice.helper.Act_ConnectionDetector;
import com.globalenterprice.helper.Constant;
import com.globalenterprice.helper.ListViewAdapter;
import com.globalenterprisesgroup.R;

public class Act_ContactUs extends Activity implements OnClickListener {

	ListView lv_contact_us;
	int arr_office[] = { R.drawable.first_office, R.drawable.sec_office,
			R.drawable.third_office, R.drawable.fourth_office,
			R.drawable.fifthoffice };
	ImageView iv_facebook, iv_twitter, iv_linkedin, iv_youtube, iv_weibo,
			iv_back, iv_wechat;
	Intent intent;
	ArrayList<String> latitudes;
	ArrayList<String> longitudes;
	private TextView tv_title;
	Act_ConnectionDetector _conn_detector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.act_contactus);
		init();
		final String[] names = getResources().getStringArray(
				R.array.office_name);
		final String[] phone = getResources().getStringArray(
				R.array.office_phone);
		final String[] email = getResources().getStringArray(
				R.array.office_email);
		final String[] locations = getResources().getStringArray(
				R.array.office_location);
		String[] address = getResources()
				.getStringArray(R.array.office_address);
		lv_contact_us = (ListView) findViewById(R.id.lv_contactus);
		lv_contact_us.setAdapter(new ListViewAdapter(Act_ContactUs.this, names,
				phone, email, address, arr_office));
		lv_contact_us.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Intent mapactivity = new Intent(Act_ContactUs.this,
						Act_GMap.class);
				mapactivity.putExtra(Constant.TITLE, names[position]);
				mapactivity.putExtra(Constant.LATITUDE, latitudes.get(position));
				mapactivity.putExtra(Constant.LONGITUDE,
						longitudes.get(position));
				startActivity(mapactivity);
			}
		});
	}

	private void init() {
		_conn_detector = new Act_ConnectionDetector(Act_ContactUs.this);

		iv_facebook = (ImageView) findViewById(R.id.iv_facebook);
		iv_linkedin = (ImageView) findViewById(R.id.iv_linkeddin);
		iv_twitter = (ImageView) findViewById(R.id.iv_tweeter);
		iv_weibo = (ImageView) findViewById(R.id.iv_weibo);
		iv_youtube = (ImageView) findViewById(R.id.iv_youtube);
		iv_wechat = (ImageView) findViewById(R.id.iv_wechat);
		iv_back = (ImageView) findViewById(R.id.iv_back);
		tv_title = (TextView) findViewById(R.id.tv_title);
		tv_title.setText(getResources().getString(R.string.contact_us));
		// Adding latlongs for offices
		latitudes = new ArrayList<String>();
		longitudes = new ArrayList<String>();
		latitudes.add("-31.95376");
		latitudes.add("39.129766");
		latitudes.add("3.160253");
		latitudes.add("51.502662");
		latitudes.add("1.279246");
		latitudes.add("-37.816077");

		longitudes.add("115.854168");
		longitudes.add("117.203042");
		longitudes.add("101.717307");
		longitudes.add("-0.020514");
		longitudes.add("103.854112");
		longitudes.add("144.958493");

		iv_back.setOnClickListener(this);
		iv_facebook.setOnClickListener(this);
		iv_linkedin.setOnClickListener(this);
		iv_twitter.setOnClickListener(this);
		iv_weibo.setOnClickListener(this);
		iv_youtube.setOnClickListener(this);
		iv_wechat.setOnClickListener(this);
	}

	private void callIntent() {
		if (_conn_detector.isConnectingToInternet())

			startActivity(intent);
		else
			Toast.makeText(getBaseContext(),
					getResources().getString(R.string.no_internet),
					Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Act_CheckList.isFromCheckList = false;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.iv_wechat:
			intent = new Intent(Act_ContactUs.this, Act_wechat.class);

			callIntent();
			break;
		case R.id.iv_facebook:
			intent = new Intent(Act_ContactUs.this, Act_WebView.class);
			intent.putExtra(Constant.URL, Constant.FACEBOOKURL);
			intent.putExtra(Constant.TITLE,
					getResources().getString(R.string.facebook));
			callIntent();

			break;
		case R.id.iv_linkeddin:
			intent = new Intent(Act_ContactUs.this, Act_WebView.class);
			intent.putExtra(Constant.URL, Constant.LINKEDINURL);
			intent.putExtra(Constant.TITLE,
					getResources().getString(R.string.linkedin));
			callIntent();

			break;
		case R.id.iv_tweeter:
			intent = new Intent(Act_ContactUs.this, Act_WebView.class);
			intent.putExtra(Constant.URL, Constant.TWITTERURL);
			intent.putExtra(Constant.TITLE,
					getResources().getString(R.string.twitter));
			callIntent();

			break;
		case R.id.iv_youtube:
			intent = new Intent(Act_ContactUs.this, Act_WebView.class);
			intent.putExtra(Constant.URL, Constant.YOUTUBEURL);
			intent.putExtra(Constant.TITLE,
					getResources().getString(R.string.youtube));
			callIntent();

			break;
		case R.id.iv_weibo:
			intent = new Intent(Act_ContactUs.this, Act_WebView.class);
			intent.putExtra(Constant.URL, Constant.WEIBOURL);
			intent.putExtra(Constant.TITLE,
					getResources().getString(R.string.weibo));
			callIntent();
			break;
		case R.id.iv_back:
			Act_CheckList.isFromCheckList = false;
			finish();
			break;

		default:
			break;
		}

	}

}
