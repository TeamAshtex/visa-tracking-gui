package com.globalenterprisesgroup;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONObject;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.globalenterprice.helper.Act_ConnectionDetector;
import com.globalenterprice.helper.Async_Task;
import com.globalenterprice.helper.Constant;
import com.globalenterprice.helper.DatabaseHelper;
import com.globalenterprice.helper.MessageCenterAdapter;
import com.globalenterprice.helper.Message_Data;
import com.globalenterprice.helper.TimeUpdate;
import com.globalenterprisesgroup.R;
public class Act_Message extends Activity {

	TextView tv_title, tv_send;
	ImageView iv_back, iv_refresh;
	ArrayList<Message_Data> messages;
	MessageCenterAdapter adapter;
	EditText text;

	static String newMessage;
	boolean isme = true;
	private String message = "", casenumber = "", timevar = "", temp_timestamp;
	DatabaseHelper dbHelper;
	Act_ConnectionDetector _conn;
	Message_Data msgmine;
	ProgressBar pb, pb1;
	SharedPreferences timeprefs, col_timestamp_pref;
	boolean ismine = true;
	public static String isFromNotificationMessage = "false";
	public static boolean isFromMessageBack = false;

	private boolean iscalled = false;
	public static boolean notificationflag = false;
	public static Act_Message act_message;

	ListView lv_messages;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.act_message);
		init();
	}

	private void init() {
		act_message = this;
		tv_title = (TextView) findViewById(R.id.tv_title);
		tv_send = (TextView) findViewById(R.id.btn_send);
		tv_title.setText(getResources().getString(R.string.message_center));
		iv_back = (ImageView) findViewById(R.id.iv_back);
		iv_refresh = (ImageView) findViewById(R.id.iv_refresh);
		text = (EditText) this.findViewById(R.id.text);
		messages = new ArrayList<Message_Data>();
		pb1 = (ProgressBar) findViewById(R.id.pb_refresh_process);
		lv_messages = (ListView) findViewById(R.id.list_messages);
		message = getIntent().getStringExtra(Constant.MESSAGE);

		isFromNotificationMessage = getIntent().getStringExtra(
				"messagenotification");

		if (isFromNotificationMessage == null)
			isFromNotificationMessage = "false";
		casenumber = getIntent().getStringExtra(Constant.MESSAGECASNUMBER);

		col_timestamp_pref = getSharedPreferences(
				Constant.COLLECTIONTIMESTAMPSHAREDPREF, MODE_PRIVATE);
		timevar = col_timestamp_pref.getString(casenumber, "0");

		/*
		 * if (message != null && !message.equals("")) { text.setText(message);
		 * }
		 */

		tv_send.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (_conn.isConnectingToInternet()) {
					sendMessage(v);
				} else {
					Toast.makeText(getBaseContext(),
							getResources().getString(R.string.no_internet),
							Toast.LENGTH_SHORT).show();

				}

			}
		});
		iv_refresh.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				iscalled = true;

				// iv_refresh.setVisibility(View.INVISIBLE);
				getmessages(casenumber);
			}
		});
		iv_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Act_CheckList.isFromCheckList = false;

				isFromMessageBack = true;
				if (casenumber.equals(Constant.NOTIFICATION_CASE_ID))
					notificationflag = false;
				else
					notificationflag = true;
				finish();
			}
		});
	}

	public void getmessages(String caseNo) {
		_conn = new Act_ConnectionDetector(Act_Message.this);
		if (_conn.isConnectingToInternet()) {

			if (!caseNo.equals(""))
				callMessageGetApi();

		} else {
			Toast.makeText(getBaseContext(),
					getResources().getString(R.string.no_internet),
					Toast.LENGTH_SHORT).show();

		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Act_CheckList.isFromCheckList = false;
		isFromMessageBack = true;
		if (casenumber.equals(Constant.NOTIFICATION_CASE_ID))
			notificationflag = false;
		else
			notificationflag = true;

	}

	public void sendMessage(View v) {
		newMessage = text.getText().toString().trim();
		if (newMessage.length() > 0) {

			text.setText("");
			tv_send.setEnabled(false);
			msgmine = new Message_Data();
			msgmine.setMessage(newMessage);
			msgmine.setMine(true);
			msgmine.setCaseNumber(casenumber);
			msgmine.setStatusMessage(false);
			msgmine.setName("Customer");
			/*
			 * SimpleDateFormat sdfDate = new SimpleDateFormat(
			 * "yyyy-MM-dd'T'HH:mm:ss'Z'");
			 */

			Date now = new Date();
			// String strDate = sdfDate.format(now);
			msgmine.setTime("");

			messages.add(msgmine);
			msgmine.issend = true;

			if (adapter != null)
				adapter.notifyDataSetChanged();
			else {
				adapter = new MessageCenterAdapter(Act_Message.this, messages);
				lv_messages.setAdapter(adapter);

			}

			lv_messages.post(new Runnable() {
				@Override
				public void run() {
					// Select the last row so it will scroll into view...
					if (adapter.getCount() > 0) {
						lv_messages.setSelection(adapter.getCount() - 1);
						lv_messages
								.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
					}
				}
			});

			isme = false;
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(text.getWindowToken(), 0);

			if (_conn.isConnectingToInternet()) {
				callsendMessageAPI();
			} else {
				Toast.makeText(getBaseContext(),
						getResources().getString(R.string.no_internet),
						Toast.LENGTH_SHORT).show();

			}

		}
	}

	private void callsendMessageAPI() {
		try {

			iv_refresh.setEnabled(false);
			new Async_Task(this, false).execute(casenumber,
					URLEncoder.encode(newMessage), Act_Message.this,
					"messagesend");
		} catch (Exception e) {

		}

	}

	private void callMessageGetApi() {
		try {
			iv_refresh.setVisibility(View.INVISIBLE);
			pb1.setVisibility(View.VISIBLE);
			tv_send.setEnabled(false);
			timevar = col_timestamp_pref.getString(casenumber, "0");

			new Async_Task(this, false).execute(casenumber, timevar,
					Act_Message.this, "messageget");
		} catch (Exception e) {
			Log.e("Exception for Login", e.toString());
		}

	}

	public String getTimefromMill(String timestamp) {
		// long time= System.currentTimeMillis();
		Date date = new Date(timestamp);
		date.toGMTString();
		// date.setTime(time);
		DateFormat formatter = new SimpleDateFormat("HH:mm");
		formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
		String dateFormatted = formatter.format(date);
		return dateFormatted;
	}

	public void jsonResponse(String result) {
		JSONObject jsonResult = null;
		JSONArray jsonarray = null;
		JSONObject jstatus = null;
		Message_Data data;
		iscalled = false;

		try {
			pb1.setVisibility(View.GONE);
			tv_send.setEnabled(true);
			iv_refresh.setVisibility(View.VISIBLE);
			jsonResult = new JSONObject(result);
			jsonarray = jsonResult.optJSONArray(Constant.RESPONSE);
			if (jsonarray != null) {
				for (int i = 0; i < jsonarray.length(); i++) {
					JSONObject jobj = jsonarray.getJSONObject(i);
					// JSONObject attr =
					// jobj.getJSONObject(Constant.ATTRIBUTES);
					// String type = attr.getString(Constant.MESSAGETYPE);
					// String url = attr.getString(Constant.MESSAGEURL);
					// String parentid = jobj.getString(Constant.PARENTID);
					String body = jobj.getString(Constant.BODY);
					// String id = jobj.getString(Constant.ID);
					String name = jobj.getString(Constant.NAME);
					String createddate = jobj.getString(Constant.CREATEDDATE);

					// long timeinterval=getTimeinMillis(createddate);
					// String timestamp = TimeUpdate.timeset(createddate);
					if (i == jsonarray.length() - 1) {

						temp_timestamp = String
								.valueOf(getTimeinMillis(createddate));
						col_timestamp_pref = getSharedPreferences(
								Constant.COLLECTIONTIMESTAMPSHAREDPREF,
								MODE_PRIVATE);
						Editor editor = col_timestamp_pref.edit();
						editor.putString(casenumber, temp_timestamp);
						editor.commit();
						// Log.e("Timestamp From Get Response", temp_timestamp);

					}

					data = new Message_Data();
					data.setName(name);
					// data.setTimestamp(timestamp);
					// data.setTimeInterval(timeinterval);

					if (name.equalsIgnoreCase("Customer")) {
						data.setMine(true);
						data.setStatusMessage(false);
					} else {
						data.setMine(false);
						data.setStatusMessage(true);
					}

					/*
					 * if (ismine) { data.setMine(false); ismine = false;
					 * data.setStatusMessage(true); } else { data.setMine(true);
					 * ismine = true; data.setStatusMessage(false); }
					 */
					data.setMessage(body);
					data.setTime(createddate);
					// data.setMessageurl(url);
					// data.setParentId(parentid);
					data.setCaseNumber(casenumber);
					// data.setType(type);
					messages.add(data);
					dbHelper.addMessage(data, Act_Message.this);

				}

				if (adapter != null)
					adapter.notifyDataSetChanged();
				else {
					adapter = new MessageCenterAdapter(Act_Message.this,
							messages);
					lv_messages.setAdapter(adapter);
				}
			} else {
				iv_refresh.setEnabled(true);
				jstatus = jsonResult.getJSONObject(Constant.RESPONSE);
				String status = jstatus.getString(Constant.STATUS);
				if (status.equalsIgnoreCase("true")) {
					// pb.setVisibility(View.GONE)
					msgmine.issend = false;
					adapter.returnmessages().set(adapter.getCount() - 1,
							msgmine);

					adapter.notifyDataSetChanged();
					tv_send.setEnabled(true);
					String sendcreateddate = jstatus
							.getString(Constant.SENDCREATEDATE);

					long timeinterval = getsendingTimeinMillis(sendcreateddate);
					msgmine.setTimeInterval(timeinterval);
					msgmine.setTime(sendcreateddate);
					String send_timestamp = TimeUpdate.timeset(sendcreateddate,
							Act_Message.this);
					msgmine.setTimestamp(send_timestamp);
					dbHelper.addMessage(msgmine, Act_Message.this);

					col_timestamp_pref = getSharedPreferences(
							Constant.COLLECTIONTIMESTAMPSHAREDPREF,
							MODE_PRIVATE);

					String timestamp = String
							.valueOf(getsendingTimeinMillis(sendcreateddate));
					// Log.e("Time Stamp Sending Response", timestamp);
					Editor editor = col_timestamp_pref.edit();

					editor.putString(casenumber, timestamp);
					editor.commit();

				} else {
					// pb.setVisibility(View.GONE);
					tv_send.setEnabled(true);
					messages.remove(messages.indexOf(msgmine));
					adapter.notifyDataSetChanged();
					lv_messages.post(new Runnable() {
						@Override
						public void run() {
							if (adapter.getCount() > 0)
								lv_messages.setSelection(lv_messages
										.getAdapter().getCount() - 1);
						}
					});
				}
			}

		} catch (Exception e) {

		}
	}

	private long getsendingTimeinMillis(String date) {
		long timeinmill = 0;

		TimeZone utc = TimeZone.getTimeZone("UTC");
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'z'");
		f.setTimeZone(utc);
		GregorianCalendar cal = new GregorianCalendar(utc);
		try {
			cal.setTime(f.parse(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		timeinmill = cal.getTimeInMillis() / 1000;

		return timeinmill;

	}

	private long getTimeinMillis(String date) {
		long timeinmill = 0;

		TimeZone utc = TimeZone.getTimeZone("UTC");
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'z'");
		f.setTimeZone(utc);
		GregorianCalendar cal = new GregorianCalendar(utc);
		try {
			cal.setTime(f.parse(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		timeinmill = cal.getTimeInMillis() / 1000;

		return timeinmill;

	}

	private String getFormattedDateTime(String datetime) {
		String formated_datetime = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
		try {
			Date d = sdf.parse(datetime);
			formated_datetime = output.format(d);
			// final_datetime=TimeUpdate.timeset(formated_datetime);
		} catch (Exception e) {

		}
		return formated_datetime;
		/*
		 * SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
		 * "yyyy-MM-dd HH:mm:ss");
		 * simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC")); String
		 * format = simpleDateFormat.format(new Date()); Log.e("TimeStamp",
		 * format); return format;
		 */
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		Log.e("In new intent", "In new intent");

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		if (getIntent().getExtras() != null) {
			casenumber = getIntent().getExtras().getString(
					Constant.MESSAGECASNUMBER);

		}

		if (!iscalled) {

			iv_refresh.setVisibility(View.VISIBLE);
			try {
				dbHelper = new DatabaseHelper(getApplicationContext());

				dbHelper.getWritableDatabase();
				if (messages.size() > 0)
					messages.clear();
				messages = dbHelper.getMessages(casenumber);
				for (int i = 0; i < messages.size(); i++) {
					messages.get(i).setTimestamp(
							TimeUpdate.timeset(messages.get(i).getTime(),
									Act_Message.this));
				}
				adapter = new MessageCenterAdapter(this, messages);
				lv_messages.setAdapter(adapter);
				// setListAdapter(adapter);
			} catch (Exception e) {

			}

			getmessages(casenumber);

			iscalled = true;
		}

	}

	public void setnotificationflag(boolean flag) {
		notificationflag = flag;
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		act_message = null;
	}

}
