package com.globalenterprisesgroup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.globalenterprice.helper.Act_ConnectionDetector;
import com.globalenterprice.helper.Constant;
import com.globalenterprisesgroup.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

//com.globalenterprisesgroup

public class Act_GMap extends Activity implements OnClickListener,OnMapReadyCallback {
	private GoogleMap googlemap;
	private double latitude, longitude;
	ImageView iv_facebook, iv_twitter, iv_linkedin, iv_youtube, iv_weibo,
			iv_back, iv_wechat;
	Intent intent;
	String title;
	TextView tv_title;
	Act_ConnectionDetector _conn_detector;
	MapFragment mapFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.act_gmap);
		init();
		try {
			initilizeMap();
		} catch (Exception e) {

		}
	}
	@Override
	public void onMapReady(GoogleMap map){

		LatLng latlng = new LatLng(latitude, longitude);
		MarkerOptions marker = new MarkerOptions().position(
				new LatLng(latitude, longitude)).title(title);

		// adding marker
		marker.icon(BitmapDescriptorFactory
				.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
		map.addMarker(marker);
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 8));

		// check if map is created successfully or not
		if (map == null) {
			Toast.makeText(getApplicationContext(),
					"Sorry! unable to create maps", Toast.LENGTH_SHORT)
					.show();
		}
	}

	private void init() {
		_conn_detector = new Act_ConnectionDetector(Act_GMap.this);
		tv_title = (TextView) findViewById(R.id.tv_title);
		iv_back = (ImageView) findViewById(R.id.iv_back);

		title = getIntent().getStringExtra(Constant.TITLE);
		tv_title.setText(title);
		latitude = Double.parseDouble(getIntent().getStringExtra(
				Constant.LATITUDE));
		longitude = Double.parseDouble(getIntent().getStringExtra(
				Constant.LONGITUDE));
		iv_facebook = (ImageView) findViewById(R.id.iv_facebook);
		iv_linkedin = (ImageView) findViewById(R.id.iv_linkeddin);
		iv_twitter = (ImageView) findViewById(R.id.iv_tweeter);
		iv_weibo = (ImageView) findViewById(R.id.iv_weibo);
		iv_youtube = (ImageView) findViewById(R.id.iv_youtube);
		iv_wechat = (ImageView) findViewById(R.id.iv_wechat);
		iv_facebook.setOnClickListener(this);
		iv_linkedin.setOnClickListener(this);
		iv_twitter.setOnClickListener(this);
		iv_weibo.setOnClickListener(this);
		iv_youtube.setOnClickListener(this);
		iv_back.setOnClickListener(this);
		iv_wechat.setOnClickListener(this);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (mapFragment != null)
			mapFragment.onPause();

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (mapFragment != null)
			mapFragment.onResume();

	}

	

	private void initilizeMap() {
		googlemap = null;
		if (googlemap == null) {
			mapFragment = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.map));
			 mapFragment.getMapAsync(this);
//			LatLng latlng = new LatLng(latitude, longitude);
//			MarkerOptions marker = new MarkerOptions().position(
//					new LatLng(latitude, longitude)).title(title);
//
//			// adding marker
//			marker.icon(BitmapDescriptorFactory
//					.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
//			googlemap.addMarker(marker);
//			googlemap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 8));
//
//			// check if map is created successfully or not
//			if (googlemap == null) {
//				Toast.makeText(getApplicationContext(),
//						"Sorry! unable to create maps", Toast.LENGTH_SHORT)
//						.show();
//			}
		}
	}


	private void callIntent() {
		if (_conn_detector.isConnectingToInternet())

			startActivity(intent);
		else
			Toast.makeText(getBaseContext(),
					getResources().getString(R.string.no_internet),
					Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.iv_wechat:
			intent = new Intent(Act_GMap.this, Act_wechat.class);

			callIntent();
			break;
		case R.id.iv_facebook:
			intent = new Intent(Act_GMap.this, Act_WebView.class);
			intent.putExtra(Constant.URL, Constant.FACEBOOKURL);
			intent.putExtra(Constant.TITLE,
					getResources().getString(R.string.facebook));
			callIntent();

			break;
		case R.id.iv_linkeddin:
			intent = new Intent(Act_GMap.this, Act_WebView.class);
			intent.putExtra(Constant.URL, Constant.LINKEDINURL);
			intent.putExtra(Constant.TITLE,
					getResources().getString(R.string.linkedin));
			callIntent();

			break;
		case R.id.iv_tweeter:
			intent = new Intent(Act_GMap.this, Act_WebView.class);
			intent.putExtra(Constant.URL, Constant.TWITTERURL);
			intent.putExtra(Constant.TITLE,
					getResources().getString(R.string.twitter));
			callIntent();

			break;
		case R.id.iv_youtube:
			intent = new Intent(Act_GMap.this, Act_WebView.class);
			intent.putExtra(Constant.URL, Constant.YOUTUBEURL);
			intent.putExtra(Constant.TITLE,
					getResources().getString(R.string.youtube));
			callIntent();

			break;
		case R.id.iv_weibo:
			intent = new Intent(Act_GMap.this, Act_WebView.class);
			intent.putExtra(Constant.URL, Constant.WEIBOURL);
			intent.putExtra(Constant.TITLE,
					getResources().getString(R.string.weibo));
			callIntent();
			break;
		case R.id.iv_back:
			finish();

			break;
		}
	}

}
