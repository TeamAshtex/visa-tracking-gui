package com.globalenterprisesgroup;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.globalenterprice.helper.Constant;
import com.globalenterprice.helper.ViewPagerAdapter;
import com.globalenterprisesgroup.R;

public class Act_Help extends Activity {

	ViewPager viewPager;
	LinearLayout linIndicator;
	private static int NUM_VIEWS = 5;
	PagerAdapter adapter;

	TextView tv_description, tv_skip;
	private String[] page_description;
	int count = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.act_help1);
		init();
	}

	private void init() {

		viewPager = (ViewPager) findViewById(R.id.view_pager);
		tv_skip = (TextView) findViewById(R.id.tv_skip);
		page_description = getResources().getStringArray(
				R.array.pager_description);
		tv_description = (TextView) findViewById(R.id.tv_descrption);
		linIndicator = (LinearLayout) findViewById(R.id.lin_indicator);
		adapter = new ViewPagerAdapter(Act_Help.this);
		viewPager.setAdapter(adapter);
		setIndicatorView(NUM_VIEWS);
		tv_skip.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(Act_Help.this, Act_Login.class));
				finish();
			}
		});

		viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrolled(int position, float positionOffset,
					int positionOffsetPixels) {
				// TODO Auto-generated method stub
				if (position == 4) {
					tv_skip.setText(getResources().getString(R.string.start));
				} else {
					tv_skip.setText(getResources().getString(R.string.Skip));
				}

				setIndicator(position);
				tv_description.setText(page_description[position]);
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});
	}

	public void setIndicator(int position) {
		int n = linIndicator.getChildCount();
		for (int i = 0; i < n; i++) {

			if (position == i) {
				linIndicator.getChildAt(i).setBackgroundResource(
						R.drawable.selected);
			} else
				linIndicator.getChildAt(i).setBackgroundResource(
						R.drawable.un_selected);

		}

	}

	public void setIndicatorView(int n) {
		linIndicator.removeAllViews();
		for (int i = 0; i < n; i++) {
			ImageView ind = new ImageView(this);
			ind.setBackgroundResource(R.drawable.selected);
			LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			param.setMargins(
					getResources().getDimensionPixelSize(
							R.dimen.indicator_padding), 0, 0, 0);
			linIndicator.addView(ind, param);
		}
	}

}
