package com.classes;

import com.globalenterprice.helper.Constant;
import com.globalenterprisesgroup.R;

import com.surveymonkey.surveymonkeyandroidsdk.SurveyMonkey;
import com.surveymonkey.surveymonkeyandroidsdk.utils.SMError;


import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class SurveryMonkeyModel {

	public static final int SM_REQUEST_CODE = 0;
	public static final String SM_RESPONDENT = "smRespondent";
	public static final String SM_ERROR = "smError";
	public static final String RESPONSES = "responses";
	public static final String QUESTION_ID = "question_id";
	public static final String FEEDBACK_QUESTION_ID = "813797519";
	public static final String ANSWERS = "answers";
	public static final String ROW_ID = "row_id";
	public static final String FEEDBACK_FIVE_STARS_ROW_ID = "9082377273";
	public static final String FEEDBACK_POSITIVE_ROW_ID_2 = "9082377274";
	public static final String SAMPLE_APP = "Sample App";
/*	public static final String SURVEY_HASH = "R3XMTW3";
*/	private SurveyMonkey s;
	private Activity activity;

	public void activityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == this.activity.RESULT_OK) {
			boolean isPromoter = false;
			Toast t = Toast.makeText(activity,
					activity.getString(R.string.thanks_prompt),
					Toast.LENGTH_LONG);
			t.show();
			String respondent = data.getStringExtra(SM_RESPONDENT);
			
			
		} else {
	/*		SMError e = (SMError) data.getSerializableExtra(SM_ERROR);
			Log.d("SM-ERROR", e.getDescription());*/
		}
	}

	public SurveryMonkeyModel(Activity activity) {
		this.activity = activity;
		s = new SurveyMonkey();
		s.onStart(activity, SAMPLE_APP, SM_REQUEST_CODE, Constant.CASE_HASHKEY);
	
		s.startSMFeedbackActivityForResult(activity, SM_REQUEST_CODE,
				Constant.CASE_HASHKEY);

	}
}
