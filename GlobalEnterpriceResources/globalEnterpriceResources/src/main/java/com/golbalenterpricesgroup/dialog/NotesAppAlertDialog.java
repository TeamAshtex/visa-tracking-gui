package com.golbalenterpricesgroup.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.globalenterprisesgroup.R;

public class NotesAppAlertDialog {

	Activity activity;
	String notes;
	Dialog dialog = null;
	TextView tv_notes, btn_ok;

	public NotesAppAlertDialog(Activity activity, String notes) {
		// TODO Auto-generated constructor stub

		this.activity = activity;
		this.notes = notes;

	}

	public void notes_alt() {

		dialog = new Dialog(activity,
				android.R.style.Theme_Translucent_NoTitleBar);
		LayoutInflater infl = (LayoutInflater) activity
				.getSystemService(activity.getApplicationContext().LAYOUT_INFLATER_SERVICE);
		View v = infl.inflate(R.layout.ly_checklist_notes,
				(ViewGroup) activity.findViewById(R.id.root));

		tv_notes = (TextView) v.findViewById(R.id.tv_notes);
		tv_notes.setText(notes);
		btn_ok = (TextView) v.findViewById(R.id.btn_ok);
		dialog.setContentView(v);

		btn_ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(dialog.getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

		dialog.show();
		dialog.getWindow().setAttributes(lp);
	}
}
