package com.globalenterprice.rssparsing;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import android.text.Html;
import android.util.Log;
import android.util.Xml;

public class XMLHandler extends DefaultHandler {
	private RssFeedStructure feedStr = new RssFeedStructure();
	private List<RssFeedStructure> rssList = new ArrayList<RssFeedStructure>();
	private static final int ARTICLES_LIMIT = 25;
	StringBuffer chars;
	private int articlesAdded = 0;

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		// TODO Auto-generated method stub
		chars.append(new String(ch, start, length));
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		// TODO Auto-generated method stub
		//Log.e("local name", localName);

		if (localName.equalsIgnoreCase("description")) {
		}
		if (localName.equalsIgnoreCase("item")) {

		}
		if (localName.equalsIgnoreCase("link")) {

		}

		if (localName.equalsIgnoreCase("title")) {
			feedStr.setTitle(chars.toString());
		} else if (localName.equalsIgnoreCase("description")) {

			feedStr.setDescription(chars.toString());

		} else if (localName.equalsIgnoreCase("pubDate")) {

			feedStr.setPubDate(chars.toString());
		} else if (localName.equalsIgnoreCase("encoded")) {
			//Log.e("Encoded Descrption", chars.toString());
			feedStr.setEncodedContent(chars.toString());
		}
		if (localName.equalsIgnoreCase("item")) {
			rssList.add(feedStr);

			feedStr = new RssFeedStructure();
			articlesAdded++;
			if (articlesAdded >= ARTICLES_LIMIT) {
				throw new SAXException();
			}
		}
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes atts) throws SAXException {
		// TODO Auto-generated method stub
		chars = new StringBuffer();

	}

	public List<RssFeedStructure> getLatestArticles(String feedUrl) {
		URL url = null;
		try {

			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();
			url = new URL(feedUrl);
			xr.setContentHandler(this);
			// android.util.Xml.parse(url.openStream(),Xml.Encoding.UTF_8,
			// this);
			xr.parse(new InputSource(url.openStream()));
		} catch (IOException e) {
		} catch (SAXException e) {

		} catch (ParserConfigurationException e) {

		}

		return rssList;
	}
}
