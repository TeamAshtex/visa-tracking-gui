package com.globalenterprice.rssparsing;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.MailTo;
import android.net.Uri;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.globalenterprice.helper.Constant;
import com.globalenterprisesgroup.Act_Full_Descrption;
import com.globalenterprisesgroup.Act_WebView;
import com.globalenterprisesgroup.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class BlogNewsAdapter extends BaseAdapter {
	private Context context;
	List<RssFeedStructure> imageAndTexts1 = null;
	public static WebView wv;
	private String isfrom;
	DisplayImageOptions options;
	ImageLoader imageLoader;
	private boolean onResume = true;

	public boolean isOnResume() {
		return onResume;
	}

	public void setOnResume(boolean onResume) {
		this.onResume = onResume;
		notifyDataSetChanged();
	}

	public BlogNewsAdapter() {

	}

	public BlogNewsAdapter(Context c, List<RssFeedStructure> objects,
			String isfrom) {
		// TODO Auto-generated constructor stub
		context = c;
		imageAndTexts1 = objects;
		this.isfrom = isfrom;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return imageAndTexts1.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.blog_news_item, parent, false);
			holder = new ViewHolder();
			assert view != null;
			options = new DisplayImageOptions.Builder().cacheInMemory(true)
			/*
			 * .showImageOnLoading(R.drawable.placeholder)
			 * .showImageForEmptyUri(R.drawable.placeholder)
			 * .showImageOnFail(R.drawable.placeholder)
			 */
			.cacheOnDisk(true).considerExifParams(true)
					.bitmapConfig(Bitmap.Config.RGB_565).build();

			imageLoader = ImageLoader.getInstance();

			holder.tv_title = (TextView) view.findViewById(R.id.tv_title);
			holder.wv_description = (WebView) view
					.findViewById(R.id.wv_content);

			holder.wv_description.setBackgroundColor(0x00000000);
			if (holder.wv_description != null) {
				if (isOnResume()) {
					holder.wv_description.onResume();
				} else {
					holder.wv_description.onPause();
				}
			}
			holder.wv_description.setWebViewClient(new WebViewClient() {
				@Override
				public void onLoadResource(WebView view, String url) {
					// TODO Auto-generated method stub
					super.onLoadResource(view, url);
				}

				public boolean shouldOverrideUrlLoading(WebView view, String url) {

					String url_another = url;
					if (url.startsWith("mailto:")) {
						MailTo mt = MailTo.parse(url);
						Intent i = newEmailIntent(mt.getTo(), mt.getSubject(),
								mt.getBody(), mt.getCc());
						context.startActivity(i);
						view.reload();
					} else if (url.startsWith("tel:")) {
						Intent intent = new Intent(Intent.ACTION_DIAL, Uri
								.parse(url));
						context.startActivity(intent);
					} else {
						Intent intent = new Intent(context, Act_WebView.class);
						intent.putExtra(Constant.URL, url_another);
						intent.putExtra(Constant.TITLE,
								imageAndTexts1.get(position).getTitle());
						context.startActivity(intent);
					}
					return true;
				}

			});
			holder.tv_more = (TextView) view.findViewById(R.id.tv_viewmore);
			holder.rel_root = (RelativeLayout) view
					.findViewById(R.id.rel_mainnews);
			// holder.iv_image = (ImageView)
			// view.findViewById(R.id.iv_blogimage);
			// holder.iv_image.setVisibility(View.GONE);

			/*
			 * holder.wv_description .setOnTouchListener(new
			 * View.OnTouchListener() {
			 * 
			 * public final static int FINGER_RELEASED = 0; public final static
			 * int FINGER_TOUCHED = 1; public final static int FINGER_DRAGGING =
			 * 2; public final static int FINGER_UNDEFINED = 3;
			 * 
			 * private int fingerState = FINGER_RELEASED;
			 * 
			 * @Override public boolean onTouch(View view, MotionEvent
			 * motionEvent) {
			 * 
			 * switch (motionEvent.getAction()) {
			 * 
			 * case MotionEvent.ACTION_DOWN: if (fingerState == FINGER_RELEASED)
			 * fingerState = FINGER_TOUCHED; else fingerState =
			 * FINGER_UNDEFINED; break;
			 * 
			 * case MotionEvent.ACTION_UP: if (fingerState != FINGER_DRAGGING) {
			 * fingerState = FINGER_RELEASED;
			 * 
			 * Intent intent = new Intent(context, Act_Full_Descrption.class);
			 * if (isfrom.equals(Constant.ISFROMBLOG))
			 * intent.putExtra(Constant.MESSAGE, imageAndTexts1.get(position)
			 * .getEncodedContent()); else intent.putExtra(Constant.MESSAGE,
			 * imageAndTexts1.get(position) .getDescription());
			 * context.startActivity(intent);
			 * 
			 * } else if (fingerState == FINGER_DRAGGING) fingerState =
			 * FINGER_RELEASED; else fingerState = FINGER_UNDEFINED; break;
			 * 
			 * case MotionEvent.ACTION_MOVE: if (fingerState == FINGER_TOUCHED
			 * || fingerState == FINGER_DRAGGING) fingerState = FINGER_DRAGGING;
			 * else fingerState = FINGER_UNDEFINED; break;
			 * 
			 * default: fingerState = FINGER_UNDEFINED;
			 * 
			 * }
			 * 
			 * return false; } });
			 */holder.tv_more.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(context,
							Act_Full_Descrption.class);
					if (isfrom.equals(Constant.ISFROMBLOG)) {
						intent.putExtra(Constant.MESSAGE,
								imageAndTexts1.get(position)
										.getEncodedContent());

					} else {
						intent.putExtra(Constant.MESSAGE,
								imageAndTexts1.get(position).getDescription());
					}
					intent.putExtra(Constant.TITLE, imageAndTexts1
							.get(position).getTitle());
					context.startActivity(intent);

				}
			});

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		holder.tv_title.setText(imageAndTexts1.get(position).getTitle());

		if (isfrom.equals(Constant.ISFROMBLOG)) {
			String description = URLEncoder.encode(imageAndTexts1.get(position)
					.getDescription());
			String decoded = URLDecoder.decode(description);

			holder.wv_description.loadData(decoded, "text/html; charset=UTF-8",
					null);
		} else {

			String description = imageAndTexts1.get(position).getDescription();
			// Log.e("Description", description);

			holder.wv_description.loadData(imageAndTexts1.get(position)
					.getDescription(), "text/html; charset=UTF-8", null);

		}
		return view;
	}

	public Intent newEmailIntent(String address, String subject, String body,
			String cc) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.putExtra(Intent.EXTRA_EMAIL, new String[] { address });
		intent.putExtra(Intent.EXTRA_TEXT, body);
		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		intent.putExtra(Intent.EXTRA_CC, cc);
		intent.setType("message/rfc822");
		return intent;
	}

	static class ViewHolder {
		TextView tv_title;
		WebView wv_description;
		TextView tv_more;
		RelativeLayout rel_root;
		ImageView iv_image;

	}

	public void setenabletrue() {
		wv.setEnabled(true);
	}

}
