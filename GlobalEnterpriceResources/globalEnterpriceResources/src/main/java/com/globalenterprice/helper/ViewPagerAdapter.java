package com.globalenterprice.helper;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.globalenterprisesgroup.R;

public class ViewPagerAdapter extends PagerAdapter {
	Context context;

	LayoutInflater inflater;
	int images[] = { R.drawable.starting_stage, R.drawable.stage_clear,
			R.drawable.message_help, R.drawable.checklist_help, R.drawable.blog

	};
	ImageView iv_screenimage;

	public ViewPagerAdapter(Context context) {
		this.context = context;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return images.length;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View itemView = inflater.inflate(R.layout.viewpager_item, container,
				false);

		iv_screenimage = (ImageView) itemView.findViewById(R.id.iv_screen_pic);
		iv_screenimage.setBackgroundResource(images[position]);
		((ViewPager) container).addView(itemView);
		return itemView;
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		// TODO Auto-generated method stub
		return view == ((RelativeLayout) object);
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {

		((ViewPager) container).removeView((RelativeLayout) object);

	}

}
