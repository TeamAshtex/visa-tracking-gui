package com.globalenterprice.helper;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.Window;

import com.globalenterprisesgroup.Act_CheckList;
import com.globalenterprisesgroup.Act_Home;
import com.globalenterprisesgroup.Act_Login;
import com.globalenterprisesgroup.Act_Message;
import com.globalenterprisesgroup.R;

public class Async_Task extends AsyncTask<Object, Boolean, String> {

	Object objs;
	private ProgressDialog Dialog;
	Context context;
	Boolean flag = true, isGCM = false;
	String obj;
	String url, jsonStr, initialcall;

	public Async_Task(Context context, Boolean flag) {
		this.context = context;
		this.flag = flag;

	}

	@Override
	protected void onPreExecute() {
		if (flag) {
			Dialog = new ProgressDialog(context);
			Dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			Dialog.setMessage(context.getResources()
					.getString(R.string.loading));
			Dialog.setCancelable(false);
			Dialog.show();
		}
	}

	@Override
	protected String doInBackground(Object... urls) {
		ServiceHandler sh = new ServiceHandler();
		objs = urls[2];
		// Making a request to url and getting response
		if (objs instanceof Act_Login || objs instanceof Act_CheckList) {
			url = Constant.LOGINAPI;
			String email = "", uniqueId = "";
			try {
				email = urls[0].toString();
				uniqueId = urls[1].toString();
			} catch (Exception e) {

			}
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(Constant.EMAIL, email));
			params.add(new BasicNameValuePair(Constant.UNIQUENUMBER, uniqueId));
			params.add(new BasicNameValuePair(Constant.lang_id, Constant.lang));

			jsonStr = sh.makeServiceCall(url, ServiceHandler.GET, params);
		} else if (objs instanceof Act_Home) {

			if (urls.length > 4) {
				String gcmtype = urls[4].toString();
				if (gcmtype != null && gcmtype.equals("gcmapi")) {
					isGCM = true;
					url = Constant.GCMSERVERURL;
					String email = "", deviceid = "", devicetype = "";
					try {
						email = urls[0].toString();
						devicetype = urls[1].toString();
						deviceid = urls[3].toString();

					} catch (Exception e) {

					}
					List<NameValuePair> params = new ArrayList<NameValuePair>();
					params.add(new BasicNameValuePair(Constant.EMAIL, email));
					params.add(new BasicNameValuePair(Constant.DEVICETOKEN,
							deviceid));
					params.add(new BasicNameValuePair(Constant.DEVICETYPE,
							devicetype));
					jsonStr = sh.makeServiceCall(url, ServiceHandler.GET,
							params);

				}
			} else {

				initialcall = urls[3].toString();

				if (initialcall.equalsIgnoreCase("initialcall")
						|| initialcall.equalsIgnoreCase("aftercall")) {
					url = Constant.LOGINAPI;
					String email = "", uniqueId = "";
					try {
						email = urls[0].toString();
						uniqueId = urls[1].toString();
					} catch (Exception e) {

					}
					List<NameValuePair> params = new ArrayList<NameValuePair>();
					params.add(new BasicNameValuePair(Constant.EMAIL, email));
					params.add(new BasicNameValuePair(Constant.UNIQUENUMBER,
							uniqueId));
					params.add(new BasicNameValuePair(Constant.lang_id,
							Constant.lang));

					jsonStr = sh.makeServiceCall(url, ServiceHandler.GET,
							params);

				}
				else if (initialcall.equalsIgnoreCase("updateLang")) {
					url=Constant.UPDATELANGAPI;
					String email = "", uniqueId = "";
					try {
						email = urls[0].toString();
						uniqueId = urls[1].toString();
					} catch (Exception e) {

					}
					List<NameValuePair> params = new ArrayList<NameValuePair>();
					params.add(new BasicNameValuePair(Constant.EMAIL, email));
					params.add(new BasicNameValuePair(Constant.UNIQUENUMBER,
							uniqueId));
					params.add(new BasicNameValuePair(Constant.lang_id,
							Constant.lang));

					jsonStr = sh.makeServiceCall(url, ServiceHandler.GET,
							params);

				} else {

					url = Constant.LOGOUTAPI;

					String email = "", devicetype = "";
					try {
						email = urls[0].toString();
						devicetype = urls[1].toString();

					} catch (Exception e) {

					}
					List<NameValuePair> params = new ArrayList<NameValuePair>();
					params.add(new BasicNameValuePair(Constant.EMAIL, email));
					params.add(new BasicNameValuePair(Constant.DEVICETYPE,
							devicetype));

					jsonStr = sh.makeServiceCall(url, ServiceHandler.GET,
							params);

				}
			}

		} else if (objs instanceof Act_Message) {
			String url_type = urls[3].toString();
			if (url_type.equals("messageget")) {
				url = Constant.MESSAGERECEIVEAPI;
				String casenumber = "", timevar = "";
				try {
					casenumber = urls[0].toString();
					int index = casenumber.indexOf("#");
					String temp_string = casenumber.substring(index + 1,
							casenumber.length()).trim();
					casenumber = temp_string;

					timevar = urls[1].toString();

				} catch (Exception e) {

				}
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair(Constant.MESSAGECASNUMBER,
						casenumber));
				params.add(new BasicNameValuePair(Constant.TIMEVAR, timevar));
				jsonStr = sh.makeServiceCall(url, ServiceHandler.GET, params);
			} else {
				url = Constant.MESSAGESENDAPI;
				String casenumber = "", comments = "";
				try {
					casenumber = urls[0].toString();
					int index = casenumber.indexOf("#");
					String temp_string = casenumber.substring(index + 1,
							casenumber.length()).trim();
					casenumber = temp_string;

					comments = urls[1].toString();

				} catch (Exception e) {

				}
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair(Constant.MESSAGECASNUMBER,
						casenumber));
				params.add(new BasicNameValuePair(Constant.COMMENTS, comments));
				jsonStr = sh.makeServiceCall(url, ServiceHandler.GET, params);

			}
		}

		return jsonStr;

	}

	@Override
	protected void onPostExecute(String result) {
		if (flag) {

			if ((Dialog != null) && Dialog.isShowing()) {
				Dialog.dismiss();
				Dialog = null;
			}
		}

		if (objs instanceof Act_Login)
			((Act_Login) objs).jsonResponse(result);

		if (objs instanceof Act_CheckList)
			((Act_CheckList) objs).jsonResponse(result);

		if (objs instanceof Act_Home && isGCM == false
				&& initialcall.equalsIgnoreCase("initialcall"))
			((Act_Home) objs).jsonResponse2(result);
		if (objs instanceof Act_Home && isGCM == false
				&& initialcall.equalsIgnoreCase("aftercall") )
			((Act_Home) objs).jsonResponse(result);
		if (objs instanceof Act_Home && isGCM == false
				&& initialcall.equalsIgnoreCase("updateLang") )
			((Act_Home) objs).jsonResonseForUpdateLang(result);
		
		if (objs instanceof Act_Home
				&& isGCM == false
				&& initialcall.equalsIgnoreCase(context
						.getString(R.string.logout)))
			((Act_Home) objs).jsonResponse3(result);

		if (objs instanceof Act_Message)
			((Act_Message) objs).jsonResponse(result);
		if (objs instanceof Act_Home && isGCM) {
			isGCM = false;
			((Act_Home) objs).jsonResponse1(result);
		}

	}

}
