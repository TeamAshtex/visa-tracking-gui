package com.globalenterprice.helper;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.globalenterprisesgroup.R;

/**
 * AwesomeAdapter is a Custom class to implement custom row in ListView
 * 
 * @author Adil Soomro
 * 
 */
public class MessageCenterAdapter extends BaseAdapter {
	private Context mContext;
	private ArrayList<Message_Data> mMessages;
	boolean ismine = false;
	private List<Long> allmessages;

	public MessageCenterAdapter(Context context,
			ArrayList<Message_Data> messages) {
		super();
		this.mContext = context;
		allmessages = new ArrayList<Long>();
		this.mMessages = messages;
		
		/*for (int i = 0; i < mMessages.size(); i++) {
			Message_Data data = mMessages.get(i);
			allmessages.add(data.getTimeInterval());
		}*/

		/*Collections.sort(messages, new Comparator<Message_Data>() {

			public int compare(Message_Data lhs,Message_Data rhs) {

				try {
					SimpleDateFormat dateFormatlhs = new SimpleDateFormat(
							"yyyy-MM-dd'T'HH:mm:ssZ");
					Date convertedDatelhs = dateFormatlhs
							.parse(lhs.getTime());
					Calendar calendarlhs = Calendar.getInstance();
					calendarlhs.setTime(convertedDatelhs);

					SimpleDateFormat dateFormatrhs = new SimpleDateFormat(
							"yyyy-MM-dd'T'HH:mm:ssZ");
					Date convertedDaterhs = dateFormatrhs
							.parse(rhs.getTime());
					Calendar calendarrhs = Calendar.getInstance();
					calendarrhs.setTime(convertedDaterhs);

					if (calendarlhs.getTimeInMillis() > calendarrhs
							.getTimeInMillis()) {

						return -1;
					} else {

						return 1;

					}
				} catch (ParseException e) {

					e.printStackTrace();
				}

				return 0;
			}
		});
*/
	}

	@Override
	public int getCount() {
		return mMessages.size();
	}

	@Override
	public Object getItem(int position) {
		return mMessages.get(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Message_Data message = (Message_Data) this.getItem(position);
	//	Log.e("Last Message Time",""+mMessages.get(mMessages.size()-1).getTime());

		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.sms_row, parent, false);
			holder.message = (TextView) convertView
					.findViewById(R.id.message_text);
			holder.message_sender = (TextView) convertView
					.findViewById(R.id.message_text_another);
			holder.tv_username = (TextView) convertView
					.findViewById(R.id.user_name);
			holder.time = (TextView) convertView.findViewById(R.id.chat_time);
			holder.pb = (ProgressBar) convertView.findViewById(R.id.pb);

			// holder.pb.setVisibility(View.GONE);

			convertView.setTag(holder);
		} else
			holder = (ViewHolder) convertView.getTag();

		holder.tv_username.setText(message.getName());
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);

		if (message.issend)
			holder.pb.setVisibility(View.VISIBLE);
		else
			holder.pb.setVisibility(View.INVISIBLE);
		ismine = message.isMine();
		
		if (ismine) {
			//holder.time.setText(message.getTimestamp());
			
			holder.time.setText(TimeUpdate.timeset(message.getTime(),mContext));
			holder.message.setVisibility(View.VISIBLE);
			holder.message.setText(message.getMessage());
			holder.message_sender.setVisibility(View.INVISIBLE);
			holder.tv_username.setTextColor(mContext.getResources().getColor(
					R.color.agent_name));
			params.gravity = Gravity.LEFT;

		}

		else {
			holder.time.setText(TimeUpdate.timeset(message.getTime(),mContext));
			holder.message.setVisibility(View.GONE);
			holder.message_sender.setVisibility(View.VISIBLE);
			holder.message_sender.setText(message.getMessage());
			holder.tv_username.setTextColor(mContext.getResources().getColor(
					R.color.user_name));

			params.gravity = Gravity.RIGHT;
			params.rightMargin = 10;

		}
		holder.time.setLayoutParams(params);
		holder.tv_username.setLayoutParams(params);

		return convertView;
	}

	private static class ViewHolder {
		TextView message, message_sender, tv_username;
		TextView time;
		ProgressBar pb;

	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	public ArrayList<Message_Data> returnmessages() {
		return this.mMessages;
	}

}
