package com.globalenterprice.helper;

import java.util.ArrayList;

import com.globalenterprisesgroup.R;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CaseListAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<String> casenumbers = new ArrayList<String>();
	View viewclicked;

	public CaseListAdapter(Context c, ArrayList<String> casenumbers) {
		// TODO Auto-generated constructor stub
		context = c;
		this.casenumbers = casenumbers;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return casenumbers.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.casetracker_item, parent, false);
			holder = new ViewHolder();
			assert view != null;
			holder.tv_case = (TextView) view.findViewById(R.id.tv_case_number);
			holder.view = (View) view.findViewById(R.id.view_seperater);
			if (position == getCount())
				holder.view.setVisibility(View.GONE);

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		holder.tv_case.setText(casenumbers.get(position));
		SharedPreferences posprefs = context.getSharedPreferences(
				Constant.CASESHAREDPREF,Context.MODE_PRIVATE);

		String castext = posprefs.getString(Constant.CASESTRING, "");

		if (casenumbers.get(position).equalsIgnoreCase(castext))
			holder.tv_case.setBackgroundColor(context.getResources().getColor(
					R.color.list_Sel_color));
		else
			holder.tv_case.setBackgroundColor(context.getResources().getColor(
					R.color.list_unsel_color));

		return view;
	}

	public void setData(ArrayList<String> casenumbers) {
		this.casenumbers = casenumbers;
		notifyDataSetChanged();
	}

	static class ViewHolder {
		TextView tv_case;
		View view;

	}

}
