package com.globalenterprice.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class Roboto_Medium_EditText extends EditText {

	public Roboto_Medium_EditText(Context context) {
		super(context);
		if (!isInEditMode())
			init(context);

	}

	public Roboto_Medium_EditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (!isInEditMode())
			init(context);
	}

	public Roboto_Medium_EditText(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		if (!isInEditMode())
			init(context);
	}

	private void init(Context context) {
		Typeface face = Typeface.createFromAsset(context.getAssets(),
				"fonts/Roboto-Medium.ttf");
		this.setTypeface(face);
	}

}
