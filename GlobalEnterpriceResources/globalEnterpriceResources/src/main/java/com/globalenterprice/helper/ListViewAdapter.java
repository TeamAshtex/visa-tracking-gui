package com.globalenterprice.helper;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.globalenterprisesgroup.R;

public class ListViewAdapter extends BaseAdapter {
	private String[] names, phone, email, address;
	private int[] officephoto;
	private Context mContext;

	public ListViewAdapter(Context c, String[] names, String[] phone,
			String[] email, String[] address, int[] officephoto) {
		// TODO Auto-generated constructor stub
		this.names = names;
		this.phone = phone;
		this.email = email;
		this.address = address;
		this.officephoto = officephoto;
		mContext = c;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return names.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.list_contact_item, null);
		LinearLayout linear_email = (LinearLayout) convertView
				.findViewById(R.id.linear_email);
		LinearLayout linear_phone = (LinearLayout) convertView
				.findViewById(R.id.linear_phone);
		ImageView iv_office = (ImageView) convertView
				.findViewById(R.id.iv_office);
		TextView tv_office_name = (TextView) convertView
				.findViewById(R.id.tv_office_name);
		TextView tv_address = (TextView) convertView
				.findViewById(R.id.tv_office_address);
		TextView tv_email = (TextView) convertView.findViewById(R.id.tv_email);
		TextView tv_phone = (TextView) convertView
				.findViewById(R.id.tv_phone_number);
		ImageView iv_message = (ImageView) convertView
				.findViewById(R.id.iv_message);
		ImageView iv_phone = (ImageView) convertView
				.findViewById(R.id.iv_phone);
		linear_email.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sendemail(email[position]);
			}
		});
		linear_phone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				TelephonyManager tmanager = (TelephonyManager) mContext
						.getSystemService(Context.TELEPHONY_SERVICE);
				int simstate = tmanager.getSimState();
				if (simstate == TelephonyManager.SIM_STATE_ABSENT)
					Toast.makeText(
							mContext,
							mContext.getResources().getString(
									R.string.sim_service_is_not_available),
							Toast.LENGTH_SHORT).show();

				else
					callphone(phone[position]);
			}
		});

		iv_office.setImageResource(officephoto[position]);
		tv_office_name.setText(names[position]);
		if (!address[position].equals(""))
			tv_address.setText(address[position]);
		else
			tv_address.setVisibility(View.GONE);
		tv_email.setText(email[position]);
		tv_phone.setText(phone[position]);

		return convertView;
	}

	private void sendemail(String email) {
		Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
		emailIntent.setType("text/html");
		List<ResolveInfo> resInfo = mContext.getPackageManager()
				.queryIntentActivities(emailIntent, 0);

		if (!resInfo.isEmpty()) {
			for (ResolveInfo info : resInfo) {
				if (info.activityInfo.packageName.toLowerCase().contains(
						"email")
						|| info.activityInfo.name.toLowerCase().contains(
								"email")) {
					emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
							new String[] { email });
					emailIntent.setPackage(info.activityInfo.packageName);
					mContext.startActivity(Intent
							.createChooser(emailIntent, ""));
				}
			}
		} else {
			Intent emailIntent1 = new Intent(android.content.Intent.ACTION_SEND);
			emailIntent1.setType("plain/text");
			emailIntent1.putExtra(android.content.Intent.EXTRA_EMAIL,
					new String[] { email });
			mContext.startActivity(Intent.createChooser(emailIntent, ""));

		}

		/*
		 * Intent i = new Intent(Intent.ACTION_SEND);
		 * i.setType("message/rfc822"); i.putExtra(Intent.EXTRA_EMAIL , new
		 * String[]{email});
		 * 
		 * try { mContext.startActivity(Intent.createChooser(i,
		 * "Send mail...")); } catch (android.content.ActivityNotFoundException
		 * ex) { Toast.makeText(mContext,
		 * "There are no email clients installed.", Toast.LENGTH_SHORT).show();
		 * }
		 */
	}

	private void callphone(String phone) {
		Intent intent = new Intent(Intent.ACTION_CALL);
		intent.setData(Uri.parse("tel:" + phone));
		mContext.startActivity(intent);
	}

}
