package com.globalenterprice.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class Roboto_Regular_TextView extends TextView {

	public Roboto_Regular_TextView(Context context) {
		super(context);
		if (!isInEditMode())
			init(context);

	}

	public Roboto_Regular_TextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (!isInEditMode())
			init(context);
	}

	public Roboto_Regular_TextView(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		if (!isInEditMode())
			init(context);
	}

	private void init(Context context) {
		Typeface face = Typeface.createFromAsset(context.getAssets(),
				"fonts/Roboto-Regular.ttf");
		this.setTypeface(face);
	}

}
