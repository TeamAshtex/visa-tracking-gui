package com.globalenterprice.helper;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {

	// Logcat tag
	private static final String LOG = DatabaseHelper.class.getName();

	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "messagelist.db";

	// Tables
	private static final String TABLE_USERMESSAGE = "UserMessage";

	// Messages Table Column Names.
	private static final String CASENUMBER = "casenumber";
	private static final String MESSAGE_BODY = "message_body";

	private static final String ISMINE = "is_mine";
	private static final String ISSTATUSMESSAGE = "is_status_message";
	private static final String MESSAGETIME = "message_time";
	public static final String MESSAGE_ID = "message_id";
	public static final String NAME = "name";
	public static final String TIMESTAMP="timestamp";

	// Message ArrayList

	private ArrayList<Message_Data> allmessages = new ArrayList<Message_Data>();
	// MESSAGE Table Create Statement.

	private static final String CREATE_TABLE_USERMESSAGE = "CREATE TABLE "
			+ TABLE_USERMESSAGE + "(" + MESSAGE_ID + " INTEGER PRIMARY KEY,"
			+ CASENUMBER + " TEXT," + MESSAGE_BODY + " TEXT," + ISMINE
			+ " TEXT," + ISSTATUSMESSAGE + " TEXT," + MESSAGETIME + " TEXT,"
			+ NAME + " TEXT,"+ TIMESTAMP + " TEXT" + ");";

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub

		try {
			db.execSQL(CREATE_TABLE_USERMESSAGE);
		} catch (Exception e) {

		}

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERMESSAGE);

		onCreate(db);
	}

	// Add MESSAGE.

	public void addMessage(Message_Data message, Context c) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(CASENUMBER, message.getCaseNumber());
		values.put(MESSAGE_BODY, message.getMessage());
		values.put(NAME, message.getName());
		values.put(TIMESTAMP, message.getTimestamp());
		values.put(ISMINE, String.valueOf(message.isMine()));
		values.put(ISSTATUSMESSAGE, String.valueOf(message.isStatusMessage()));
		values.put(MESSAGETIME, message.getTime());

		db.insert(TABLE_USERMESSAGE, null, values);
		// logToDisplay(c,"Message is "+message.getMessage());
		db.close();
	}

	// Delete MESSAGE.

	public void deleteMessageDatabase(Context c) {

		c.deleteDatabase(DATABASE_NAME);

	}

	// Fetch All Messages.

	public ArrayList<Message_Data> getMessages(String casenumber) {

		SQLiteDatabase db = this.getReadableDatabase();

		String selectQuery = "SELECT * FROM " + TABLE_USERMESSAGE
				+ " WHERE casenumber = ?";

		// String selectQuery = "SELECT * FROM " + TABLE_USERMESSAGE;

		//Log.e(LOG, selectQuery);
		if (allmessages.size() > 0)
			allmessages.clear();

		Cursor c = db.rawQuery(selectQuery, new String[] { casenumber });

		if (c != null && c.moveToFirst()) {

			do {
				Message_Data message = new Message_Data();
				message.setCaseNumber((c.getString(c.getColumnIndex(CASENUMBER))));
				message.setMessage(((c.getString(c.getColumnIndex(MESSAGE_BODY)))));
				message.setTimestamp(((c.getString(c.getColumnIndex(TIMESTAMP)))));
				message.setMine((Boolean.parseBoolean(c.getString(c
						.getColumnIndex(ISMINE)))));
				message.setStatusMessage(Boolean.parseBoolean((c.getString(c
						.getColumnIndex(ISSTATUSMESSAGE)))));
				message.setTime(((c.getString(c.getColumnIndex(MESSAGETIME)))));
				message.setName(((c.getString(c.getColumnIndex(NAME)))));
				allmessages.add(message);
			} while (c.moveToNext());
		}

		return allmessages;
	}
	


}
