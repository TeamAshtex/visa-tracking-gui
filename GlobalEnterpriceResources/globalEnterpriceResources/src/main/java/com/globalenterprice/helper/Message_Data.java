package com.globalenterprice.helper;

public class Message_Data {
	String message, time, name, casenumber, timestamp;
	public boolean issend = false;
	long timeinterval;
	/**
	 * boolean to determine, who is sender of this message
	 */
	boolean isMine;

	public boolean isStatusMessage;

	/**
	 * Constructor to make a Message object
	 */
	public Message_Data(String message, boolean isMine) {
		super();
		this.message = message;
		this.isMine = isMine;
		this.isStatusMessage = false;
	}

	public Message_Data() {

	}

	/**
	 * Constructor to make a status Message object consider the parameters are
	 * swaped from default Message constructor, not a good approach but have to
	 * go with it.
	 */
	public Message_Data(boolean status, String message) {
		super();
		this.message = message;
		this.isMine = false;
		this.isStatusMessage = status;
	}

	public String getMessage() {
		return message;
	}

	public void setTimeInterval(long timeinterval) {
		this.timeinterval = timeinterval;
	}

	public long getTimeInterval() {
		return this.timeinterval;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getTimestamp() {
		return this.timestamp;
	}

	public String getTime() {
		return this.time;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isMine() {
		return isMine;
	}

	public void setMine(boolean isMine) {
		this.isMine = isMine;
	}

	public boolean isStatusMessage() {
		return isStatusMessage;
	}

	public void setStatusMessage(boolean isStatusMessage) {
		this.isStatusMessage = isStatusMessage;
	}

	public void setCaseNumber(String casenumber) {
		this.casenumber = casenumber;
	}

	public String getCaseNumber() {
		return this.casenumber;
	}

}
