package com.globalenterprice.helper;

import java.util.Locale;

import android.app.Activity;
import android.content.res.Configuration;

public class Constant {

	// http://reviewprototypes.com/projects_dev/Client%20Live%20Projects/GE/
	// General Response
	public static final String ERROR = "error";
	public static final String MESSAGE = "message";
	// public static final String
	// BASEURL="http://reviewprototypes.com/projects_dev/Client%20Live%20Projects/GE/";
	// public static final String BASEURL =
	// "http://reviewprototypes.com/projects_dev/Client%20Live%20Projects/GE/Production/api.php";

	// live
	 public static final String BASEURL =
	 "http://reviewprototypes.com/projects_dev/Client%20Live%20Projects/GE/Production/api.php";
	 public static final String PUSHBASEURL =
	 "http://reviewprototypes.com/projects_dev/Client%20Live%20Projects/GE/Production/Pushnotification/push.php";

	// devapp
	/*public static final String BASEURL = "http://reviewprototypes.com/projects_dev/Client%20Live%20Projects/GE/Sandbox/api.php";
	public static final String PUSHBASEURL = "http://reviewprototypes.com/projects_dev/Client%20Live%20Projects/GE/Sandbox/Pushnotification/push.php";
*/
	// UpdateLang API
	public static final String UPDATELANGAPI = BASEURL
			+ "?apiname=updatelanguage";
	
	public static final String LANG_ID = "lang_id";

	// Login API

	public static final String LOGINAPI = BASEURL + "?apiname=login";
	public static final String EMAIL = "email";
	public static final String UNIQUENUMBER = "uniqueNumber";
	public static final String ERRORCODE = "errorCode";

	// Logout API
	public static final String LOGOUTAPI = BASEURL + "?apiname=logout";

	// LoginPrefs

	public static final String LOGINPREF = "loginpref";
	public static final String LOGINUSEREMAIL = "loginemail";
	public static final String LOGINPASS = "loginpass";

	// Contact_Us Page

	public static final String FACEBOOKURL = "https://www.facebook.com/GlobalEnterprisesGroup";
	public static final String TWITTERURL = "https://twitter.com/GEGroupPtyLtd";
	public static final String LINKEDINURL = "http://www.linkedin.com/company/global-enterprises-group-pty-ltd";
/*	public static final String DEMOURL="https://devapp.studiobookingsonline.com/yogaparadise/login/signin";
*/	public static final String YOUTUBEURL = "http://www.youtube.com/user/GlobalEnterprisesPL?feature=watch";
	public static final String WEIBOURL = "http://weibo.com/gegroupptyltd";
	public static final String WECHATURL = "http://weixin.qq.com/r/WzpRSbHR2OynrfUq929J";

	public static final String URL = "url";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String LOCATION = "location";

	// New Bolg News (changes in Phase 2)
	public static final String BLOGURL = "http://www.globalenterprisesgroup.com/feed/";
	public static final String RESOURCESURL = "http://www.globalenterprisesgroup.com/resources/";
	public static final String INFORMATIONURL = "http://www.globalenterprisesgroup.com/loginarea/";

	// Blog News Resources URLS
	public static final String TITLE = "title";
	// public static final String BLOGURL =
	// "http://globalenterprisesgroup.com/blog/feed/";
	public static final String ISFROM = "isfrom";
	public static final String ISFROMBLOG = "fromblog";
	public static final String ISFROMNEWS = "fromnews";
	// public static final String NEWSURL =
	// "http://globalenterprisesgroup.com/blog/category/news/feed/";
	public static final String NEWSURL = "http://globalenterprisesgroup.com/blog/twitterfeeds/";
	// public static final String RESOURCESURL =
	// "http://globalenterprisesgroup.com/blog/resource/";
	// public static final String INFORMATIONURL =
	// "http://globalenterprisesgroup.com/blog/loginarea/";

	// SharedPrefernces constants
	public static final String SHARED_PREF = "sharedpreferences";
	public static final String RESPONSE = "response";
	public static final String STAGE = "Stage";
	public static final String ID = "Id";
	public static final String CHILDREN = "children";
	public static final String CASENUMBERVALUE = "CaseNumbervalue";
	public static final String CHECKLIST = "Checklist";
	public static final String NOTES = "notes";
	public static final String CHECKED = "checked";
	public static final String SIZE = "size";
	public static int position = -1;
	public static final String SURVEYHASH = "SurveyHash";

	// selection parameters
	public static final String CASESHAREDPREF = "casesharedpref";
	public static final String CASESTRING = "casestring";

	// User Data constants
	public static String EMAIL_ID;
	public static String USER_UNIQUE_ID;
	public static String CASE_ID;
	public static String CASE_HASHKEY = "";

	public static String refreshstage = "";

	public static String eng_lang = "1";
	public static String ch_lang = "2";
	public static String lang = eng_lang;
	public static String lang_id = "lang_id";

	public static class Config {
		public static final boolean DEVELOPER_MODE = false;
	}

	//
	public static String CASEIDHOME;

	// MESSAGE API CONSTANTS
	public static final String MESSAGETYPE = "type";
	public static final String NAME = "name";
	public static final String ATTRIBUTES = "attributes";
	public static final String MESSAGEURL = "url";
	public static final String PARENTID = "CreatedById";
	public static final String BODY = "body";
	public static final String CREATEDDATE = "CreatedDate";
	public static final String SENDCREATEDATE = "CreateDate";
	public static final String MESSAGECASNUMBER = "caseno";
	public static final String TIMEVAR = "timevar";
	public static final String COMMENTS = "comments";
	public static final String STATUS = "status";
	public static final String MESSAGERECEIVEAPI = BASEURL
			+ "?apiname=getmessage";
	public static final String MESSAGESENDAPI = BASEURL
			+ "?apiname=sendmessage";

	// TimeStamp Shared Preferences

	public static final String TIMESTAMPSHAREDPREF = "timestamppref";
	public static final String LASTTIMESTAMP = "lasttimestamp";
	// public static final boolean ISMINE=;
	public static final String ISSTATUSMESSAGE = "isstatusmesage";
	public static String MYMESSAGEID = "0D5N0000003pOivKAE";

	// Collections of TimeStamps case tracker wise sharepreferences

	public static final String COLLECTIONTIMESTAMPSHAREDPREF = "collection_timestamp";

	// GCM server url
	public static final String GCMSERVERURL = BASEURL
			+ "?apiname=deviceregister";
	public static final String DEVICETOKEN = "deviceToken";
	public static final String DEVICETYPE = "devicetype";
	public static String REGISTRATIONID = "";
	public static final String DEVICETYPEID = "2";

	// NOTIFICATION Constants

	public static final String NOTIFICATIONCASEID = "notification_case_id";
	public static boolean ISFROMNOTIFICATION;
	public static String NOTIFICATION_CASE_ID = "";

	// Help Screen Constants

	public static final String HELPSHAREDPREF = "helpsharedpref";
	public static final String ISHELPCALLED = "ishelpcalled";

	public static final String ISDEVICETOKEN="isDeviceToken";
	// Meesage APIs
	// http://reviewprototypes.com/projects_dev/getmessagesoap.php?caseno=00001304&timevar=1403439132
	// Send Comment :
	// reviewprototypes.com/projects_dev/testsoapapi2.php?caseno=00001304&comments=Testing

	// sample url
	// http://reviewprototypes.com/projects_dev/testsoapapi.php?email=singharjun84@gmail.com&uniqueNumber=1
	
	public static void changeLang(Activity activity) {
		String lang=Constant.lang.equals("1")?"en":"zh";
	
		
		Locale locale = new Locale(lang);
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		activity.getBaseContext().getResources().updateConfiguration(config,
				activity.getBaseContext().getResources().getDisplayMetrics());

	}

}
