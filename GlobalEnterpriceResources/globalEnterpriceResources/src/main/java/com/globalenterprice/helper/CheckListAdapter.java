package com.globalenterprice.helper;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.globalenterprisesgroup.R;
import com.golbalenterpricesgroup.dialog.NotesAppAlertDialog;

public class CheckListAdapter extends BaseAdapter {
	private Activity context;
	private ArrayList<String> checkListItems = new ArrayList<String>();
	private ArrayList<Boolean> checkedItems = new ArrayList<Boolean>();
	private ArrayList<String> checkListItemsNotes = new ArrayList<String>();

	public CheckListAdapter(Activity c, ArrayList<String> checkListItems,
			ArrayList<Boolean> checkedItems,
			ArrayList<String> checkListItemsNotes) {
		// TODO Auto-generated constructor stub
		context = c;
		this.checkListItems = checkListItems;
		this.checkedItems = checkedItems;
		this.checkListItemsNotes = checkListItemsNotes;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return checkListItems.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.checklist_item, parent, false);
			holder = new ViewHolder();
			assert view != null;
			holder.tv_checkListItem = (TextView) view
					.findViewById(R.id.tv_checklist_desc);
			holder.rb_checkList = (RadioButton) view
					.findViewById(R.id.rb_radiochecklist);
			holder.iv_notes = (ImageView) view.findViewById(R.id.iv_notes);

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		holder.tv_checkListItem.setText(checkListItems.get(position));
		if (checkedItems.get(position)) {
			holder.rb_checkList.setButtonDrawable(R.drawable.checkmark);
			Typeface tf = Typeface.createFromAsset(context.getAssets(),
					"fonts/Roboto-Regular.ttf");
			holder.tv_checkListItem.setTypeface(tf);
			holder.tv_checkListItem.setTypeface(tf);
			holder.tv_checkListItem.setTextColor(context.getResources()
					.getColor(android.R.color.black));

			// holder.rb_checkList.setBackgroundResource(R.drawable.checkmark);

		} else {
			holder.rb_checkList.setButtonDrawable(R.drawable.blank_mark);
			Typeface tf = Typeface.createFromAsset(context.getAssets(),
					"fonts/Roboto-Medium.ttf");
			holder.tv_checkListItem.setTypeface(tf);
			holder.tv_checkListItem.setTextColor(context.getResources()
					.getColor(R.color.red_color));
			// holder.rb_checkList.setBackgroundResource(R.drawable.blank_mark);

		}
		holder.iv_notes.setTag(position);
		if (checkListItemsNotes.get(position) != null) {
			if (!checkListItemsNotes.get(position).trim().equals("null")) {
				holder.iv_notes.setVisibility(View.VISIBLE);
				holder.iv_notes.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						int pos = (Integer) v.getTag();
						new NotesAppAlertDialog(context, checkListItemsNotes
								.get(pos)).notes_alt();
					}
				});
			} else {
				holder.iv_notes.setVisibility(View.GONE);
			}
		} else {
			holder.iv_notes.setVisibility(View.GONE);

		}
		return view;
	}

	static class ViewHolder {
		TextView tv_checkListItem;
		RadioButton rb_checkList;
		ImageView iv_notes;
	}

}
