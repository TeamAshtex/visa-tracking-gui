package com.globalenterprice.helper;

/*
 * Classname     :TimeUpdate
 *
 * Version info  :1.0
 * 
 * Description   :TimeUpdate class converts the time fetched to the current timezone of the user
 */
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.globalenterprisesgroup.R;

import android.content.Context;
import android.util.Log;

public class TimeUpdate {

	public static String timeset(String args, Context context) {

		Date exdate = null, date2 = null, date_am_pm = null;
		String resTime = null, convertedTime = null;

		SimpleDateFormat formatter;
		if (args.endsWith("Z"))
			formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		else if (args.endsWith("z"))
			formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'z'");
		else {
			resTime = context.getResources().getString(
					R.string.a_few_seconds_ago);

			return resTime;

		}

		String currDate = args;
		// CURRENT SYSTEM TIME

		formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

		try {
			exdate = formatter.parse(currDate);
			convertedTime = formatter.format(exdate);

		} catch (ParseException e1) {

			e1.printStackTrace();
		}

		String timezoneID = TimeZone.getDefault().getID();

		Calendar calendar = Calendar.getInstance(TimeZone
				.getTimeZone(timezoneID));
		Date SystemTime = calendar.getTime();

		long currentDate = calendar.getTimeInMillis();
		// Log.e("current time",""+currentDate);

		try {
			date2 = formatter.parse(currDate);
		} catch (ParseException e) {

			e.printStackTrace();
		}
		// Log.e("server time", ""+date2.getTime());
		long milliseconds = currentDate - date2.getTime();

		if (milliseconds < 0) {

			resTime = context.getResources().getString(R.string.error_in_time);

		}

		long time = milliseconds / 1000;
		String seconds = Integer.toString((int) (time % 60));
		String minutes = Integer.toString((int) ((time % 3600) / 60));
		String hours = Integer.toString((int) (time / 3600));
		for (int i = 0; i < 2; i++) {
			if (seconds.length() < 2) {
				seconds = "0" + seconds;
			}
			if (minutes.length() < 2) {
				minutes = "0" + minutes;
			}
			if (hours.length() < 2) {
				hours = "0" + hours;
			}
		}

		int hr = Integer.parseInt(hours);
		int min = Integer.parseInt(minutes);
		int sec = Integer.parseInt(seconds);
		int days = 0;

		int dateClient = date2.getDate();

		int systemDate = SystemTime.getDate();

		if (((systemDate - dateClient) == 1) && (hr < 24)) {
			resTime = context.getResources().getString(R.string.yesterda);
		}

		else if (hr > 24) {
			days = (hr) / 24;

			if (days > 1) {

				try {

					SimpleDateFormat formatAA = new SimpleDateFormat(
							"dd-MM-yyyy hh:mm:ss aa");
					formatAA.setTimeZone(TimeZone.getTimeZone("UTC"));
					resTime = formatAA.format(exdate);

					// resTime = convertedTime.toString();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				if (days == 1) {
					resTime = context.getResources().getString(
							R.string.yesterda);

				} else {

					resTime = "" + Math.abs(days)
							+ context.getResources().getString(R.string.dayago);
				}
			}
		} else {
			if (hr == 00) {
				if (min == 00) {

					// resTime = "A few seconds ago";
					if (sec == 0)
						resTime = context.getResources().getString(
								R.string.a_few_seconds_ago);
					else
						resTime = ""
								+ Math.abs(sec)
								+ context.getResources().getString(
										R.string.secondago);
				} else {

					if (min == 1)
						context.getResources().getString(R.string.minute_ago);
					else
						resTime = ""
								+ Math.abs(min)
								+ context.getResources().getString(
										R.string.muniteago);

				}
			}

			else if (!((systemDate - dateClient) == 1) && (hr < 24)) {

				if (hr == 1)
					resTime = ""
							+ Math.abs(hr)
							+ context.getResources().getString(
									R.string.hour_ago);
				else
					resTime = ""
							+ Math.abs(hr)
							+ context.getResources()
									.getString(R.string.hourago);

			} else if ((hr == 24)) {

				resTime = context.getResources().getString(R.string.yesterda);
			}

		}

		return resTime;
	}

}
